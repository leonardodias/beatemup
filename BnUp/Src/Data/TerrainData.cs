﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class TerrainData
    {
        public RectangleData Rectangle { get; set; }
        public string TerrainType { get; set; }
    }
}
