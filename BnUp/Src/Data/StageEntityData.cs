﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class StageEntityData
    {
        public string EntityType { get; set; }
        public string EntityFile { get; set; }
        public PointData Position { get; set; }
        public RectangleData Trigger { get; set; }        
    }
}
