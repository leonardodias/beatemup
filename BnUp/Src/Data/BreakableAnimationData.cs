﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class BreakableAnimationData
    {
        public int HP { get; set; }
        public string Name { get; set; }
    }
}
