﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class StageData
    {
        public string Name { get; set; }
        public List<BackgroundData> Backgrounds { get; set; }
        public List<TerrainData> Floors { get; set; }
        public List<TerrainData> Walls { get; set; }
        public List<PointData> PlayersStart { get; set; }
        public List<SectionData> Sections { get; set; }
        public List<StageEntityData> Entities { get; set; }
        public RectangleData CameraLimits { get; set; }
        public MusicData Music { get; set; }
    }
}
