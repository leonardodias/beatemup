﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class AttackData
    {
        public int Damage { get; set; }

        public float Delay { get; set; }

        public float Duration { get; set; }

        public float Cooldown { get; set; }

        public int Range { get; set; }

        public bool KnockDown { get; set; }

        public string AnimationName { get; set; }

        public bool ContinuousAirAttack { get; set; }

        public RectangleData AttackArea { get; set; }

    }
}
