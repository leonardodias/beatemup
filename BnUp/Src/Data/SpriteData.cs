﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class SpriteData
    {
        public string Name { get; set; }        
        public int Y { get; set; }
        public int LeftOffsetX { get; set; }
        public int RightOffsetX { get; set; }
    }
}
