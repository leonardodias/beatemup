﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class ThrowData
    {        
        public float Delay { get; set; }
        public float Cooldown { get; set; }
        public PointData EnemyPosition { get; set; }
        public int Damage { get; set; }       

    }
}
