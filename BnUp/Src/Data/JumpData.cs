﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class JumpData
    {
        public int Height { get; set; }
        public float Delay { get; set; }
        public string StartJumpAnimation { get; set; }
    }
}
