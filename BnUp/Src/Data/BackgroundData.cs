﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class BackgroundData
    {
        public string ImageName { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int ParallaxDistance { get; set; }
    }
}
