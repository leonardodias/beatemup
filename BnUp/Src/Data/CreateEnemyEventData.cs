﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class CreateEnemyEventData
    {
        public float Delay { get; set; }
        public int MinEnemiesCount { get; set; }
        public PointData SpawnPoint { get; set; }
        public PointData DestinationPoint { get; set; }
        public string Name { get; set; }
    }
}
