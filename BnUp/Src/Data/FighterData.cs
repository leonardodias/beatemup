﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class FighterData
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public SpriteData Sprite { get; set; }
        public int ShadowX { get; set; }
        public int HP { get; set; }
        public int Velocity { get; set; }
        public JumpData Jump { get; set; }
        public bool Player { get; set; }
        public bool GrabLifting { get; set; }
        public ThrowData BackThrow { get; set; }
        public ThrowData FrontThrow { get; set; }
        public bool Unstoppable { get; set; }

        public List<AttackData> StandingAttacks { get; set; }
        public List<AttackData> AirAttacks { get; set; }
        public List<AttackData> GrabAttacks { get; set; }
        
        public String SpecialMove { get; set; }
        public DashData Dash { get; set; }
    }
}
