﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class BlockData
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public SpriteData Sprite { get; set; }
        public int HP { get; set; }
        public List<BreakableAnimationData> Animations { get; set; }
    }
}
