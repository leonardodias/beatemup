﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class SectionData
    {
        public RectangleData Bounds { get; set; }
        public RectangleData Trigger { get; set; }
        public RectangleData NextLimits { get; set; }

        public List<CreateEnemyEventData> CreateEnemyEventDataList { get; set; }

    }
}
