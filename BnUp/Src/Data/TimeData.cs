﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Data
{
    class TimeData
    {
        public int Minute { get; set; }
        public int Seconds { get; set; }
        public int Milliseconds { get; set; }
    }
}
