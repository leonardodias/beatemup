﻿using Microsoft.Xna.Framework.Graphics;
using Monocle;
using BnUp.Src.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using BnUp.Src.Enums;
using Microsoft.Xna.Framework.Media;
using MonoBMFontUWP.Loader;

namespace BnUp.Src.Assets
{
    class AssetsManager
    {
        #region Fields

        //List<Atlas> Atlases;

        private static SpriteBank _spriteBank = null;

        public static SpriteBank AllSprites
        {
            get
            {
                return _spriteBank;
            }
        }

        private static Atlas _textureAtlas;

        public static Atlas TextureAtlas
        {
            get
            {
                return _textureAtlas;
            }
        }

        //Sound
        private static Dictionary<SoundKey, SoundEffect> _soundEffects = new Dictionary<SoundKey, SoundEffect>();

        public static SoundEffect GetSound(SoundKey soundKey)
        {
            return _soundEffects[soundKey];
        }

        //Music
        private Song _currentSong;

        //Fonts
        private static Dictionary<FontKey, BmFont> _fonts = new Dictionary<FontKey, BmFont>();

        public static BmFont GetFont(FontKey fontKey)
        {
            return _fonts[fontKey];
        }

        //Video
        private Video _currentVideo;

        #endregion

        #region Singleton

        private static AssetsManager _instance = null;

        public static AssetsManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AssetsManager();
                return _instance;
            }
        }

        private AssetsManager()
        {
            InitStuff();
        }

        #endregion

        private void InitStuff()
        {
            //Load Atlas
            _textureAtlas = Atlas.FromAtlas("Atlases\\atlas", Atlas.AtlasDataFormat.CrunchXmlOrBinary);
            _spriteBank = new SpriteBank(_textureAtlas, "SpriteData\\Sprites.xml");

            //Load Sounds
            _soundEffects.Add(SoundKey.PunchImpact, Engine.Instance.Content.Load<SoundEffect>(@"Sound\sfx_sounds_impact7"));

            //Load Fonts
            _fonts.Add(FontKey.ZCool32, new BmFont(@"Font\zcool32.fnt", @"Font\zcool32_0", Engine.Instance.Content));
            _fonts.Add(FontKey.ZCool24, new BmFont(@"Font\zcool24r.fnt", @"Font\zcool24r_0", Engine.Instance.Content));


        }

        /// <summary>
        /// Reloads atlases from file (should not be called so often).
        /// </summary>
        public void RefreshData()
        {
            InitStuff();
        }

        public void Dispose()
        {
            //foreach (var atlas in Atlases)
            //{
            //atlas.Dispose();
            //}
            _textureAtlas.Dispose();

            foreach (var sound in _soundEffects.Values)
            {
                sound.Dispose();
            }

            if (_currentSong != null)
            {
                _currentSong.Dispose();
            }

            foreach (var font in _fonts.Values)
            {
                font.Dispose();
            }

            if (_currentVideo != null)
            {
                _currentVideo.Dispose();
            }
        }

        public Song LoadSong(string name)
        {
            if (_currentSong != null)
                _currentSong.Dispose();

            _currentSong = Engine.Instance.Content.Load<Song>(@"Music\" + name);

            return _currentSong;
        }

        public Song GetSong()
        {
            return _currentSong;
        }

        public Video LoadVideo(string name)
        {
            if (_currentVideo != null)
                _currentVideo.Dispose();

            _currentVideo = Engine.Instance.Content.Load<Video>(@"Video\" + name);

            return _currentVideo;
        }

        public Video GetVideo()
        {
            return _currentVideo;
        }

    }
}
