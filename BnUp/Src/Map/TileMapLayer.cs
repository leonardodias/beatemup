﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Map
{
    class TileMapLayer
    {
        public int Index { get; set; }
        private List<Tile> _tiles = new List<Tile>();
        public string Tileset { get; set; }
        public List<Tile> Tiles
        {
            get { return _tiles; }
        }

        public void AddTexture(Texture2D texture)
        {
            foreach (var tile in _tiles)
            {
                tile.Texture = texture;
            }
        }

        internal void AddTIle(Tile tile)
        {
            _tiles.Add(tile);
        }
    }
}
