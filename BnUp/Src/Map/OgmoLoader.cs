﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Map
{
    class OgmoLoader
    {
        public TileMap createMap(string json)
        {
            var data = JObject.Parse(json);

            TileMap tileMap = new TileMap();

            tileMap.Width = Convert.ToInt32(data["width"]);
            tileMap.Height = Convert.ToInt32(data["width"]);

            foreach (var dataLayer in data["layers"])
            {
                TileMapLayer mapLayer = new TileMapLayer();

                mapLayer.Tileset = dataLayer["tileset"].ToString();
                
                int gridCellsX = Convert.ToInt32(dataLayer["gridCellsX"]);
                int gridCellsY = Convert.ToInt32(dataLayer["gridCellsY"]);

                int gridCellWidth = Convert.ToInt32(dataLayer["gridCellWidth"]);
                int gridCellHeight = Convert.ToInt32(dataLayer["gridCellHeight"]);


                int xPos = 0;
                //int yPos = gridCellsY - 1;
                int yPos = 0;

                foreach (var dataTile in dataLayer["dataCoords"])
                {
                    int coordX = Convert.ToInt32(dataTile[0]);

                    if (coordX != -1)
                    {
                        int coordY = Convert.ToInt32(dataTile[1]);
                        Tile tile = new Tile(xPos, yPos, coordX, coordY, gridCellWidth, gridCellHeight);
                        mapLayer.AddTIle(tile);
                    }

                    xPos++;
                    if (xPos >= gridCellsX)
                    {
                        xPos = 0;
                        yPos++;
                    }

                }

                tileMap.AddTileMapLayer(mapLayer);
            }

            return tileMap;
        }
    }
}
