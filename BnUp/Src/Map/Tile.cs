﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Map
{
    class Tile
    {
        public int TileWidth = 16;
        public int TileHeight = 16;

        public int TextureTileWidth = 16;
        public int TextureTileHeight = 16;

        public int XPos { get; set; }
        public int YPos { get; set; }        

        public int TextureXPos { get; set; }
        public int TextureYPos { get; set; }

        public Texture2D Texture { get; internal set; }

        public int WorldXPos { get; private set; }
        public int WorldYPos { get; private set; }
        public int WorldTextureXPos { get; private set; }
        public int WorldTextureYPos { get; private set; }               

        public Tile(int xPos, int yPos, int textureXPos, int textureYPos, int tileWidth, int tileHeight)
        {
            XPos = xPos;
            YPos = yPos;            
            TextureXPos = textureXPos;
            TextureYPos = textureYPos;
            TileWidth = tileWidth;
            TileHeight = tileHeight;

            WorldXPos = XPos * TileWidth;
            WorldYPos = YPos * TileHeight;
            WorldTextureXPos = TextureXPos * TextureTileWidth;
            WorldTextureYPos = TextureYPos * TextureTileHeight;
        }

    }
}
