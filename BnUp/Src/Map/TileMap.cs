﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Map
{
    class TileMap
    {
        private List<TileMapLayer> _layers = new List<TileMapLayer>();
        public int Width { get; set; }
        public int Height { get; set; }

        private List<Texture2D> _textures = new List<Texture2D>();

        public List<TileMapLayer> Layers
        {
            get { return _layers; }
        }

        public void AddTileMapLayer(TileMapLayer tileMapLayer)
        {
            _layers.Add(tileMapLayer);
        }

        internal void Dispose()
        {
            foreach (var texture in _textures)
            {
                texture.Dispose();                                
            }
        }

        public void AddTexture(string textureName, Texture2D texture)
        {
            foreach (var tileMapLayer in _layers)
            {
                if (tileMapLayer.Tileset.Equals(textureName))
                {
                    tileMapLayer.AddTexture(texture);
                }
            }

            _textures.Add(texture);
        }
    }
}
