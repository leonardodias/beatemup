﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Util
{
    class BlocksPath
    {
        private const string Path = "./Content/Data/Blocks/";
        private const string Extension = "BlockData.json";
        private Dictionary<string, string> _pathMap = new Dictionary<string, string>();

        #region Singleton

        private static BlocksPath _instance = null;

        public static BlocksPath Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BlocksPath();
                return _instance;
            }
        }

        private BlocksPath()
        {
            InitMap();
        }
        #endregion

        private void InitMap()
        {
            AddBlock("Barrel");
        }

        private void AddBlock(string name)
        {
            _pathMap.Add(name, Path + name + Extension);
        }

        public string Get(string key)
        {
            return _pathMap[key];
        }
    }
}
