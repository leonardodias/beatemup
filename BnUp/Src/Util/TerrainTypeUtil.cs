﻿using BnUp.Src.Enums;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Util
{
    class TerrainTypeUtil
    {
        public static bool CheckFloorType(Collider baseCollider, Entity terrainEntity, TerrainType type)
        {
            if (type == TerrainType.Square)
            {
                return true;
            }

            float pointX = baseCollider.Position.X - terrainEntity.Position.X;
            float pointY = baseCollider.Position.Y - terrainEntity.Position.Y;

            if (Check(pointX, pointY, terrainEntity, type))
            {
                return true;
            }

            pointX += baseCollider.Width;

            if (Check(pointX, pointY, terrainEntity, type))
            {
                return true;
            }

            pointY += baseCollider.Height;

            if (Check(pointX, pointY, terrainEntity, type))
            {
                return true;
            }

            pointX -= baseCollider.Width;

            if (Check(pointX, pointY, terrainEntity, type))
            {
                return true;
            }

            return false;
        }

        private static bool Check(float pointX, float pointY, Entity floorEntity, TerrainType type)
        {
            switch (type)
            {
                case TerrainType.Square:
                    return true;

                case TerrainType.TriangleNE:                    
                    return (pointX * floorEntity.Height >= pointY * floorEntity.Width);

                case TerrainType.TriangleNW:
                    return ((pointX * floorEntity.Height) <= ((floorEntity.Height - pointY) * floorEntity.Width));

                case TerrainType.TriangleSE:
                    return ((pointX * floorEntity.Height) >= ((floorEntity.Height - pointY) * floorEntity.Width)); ;

                case TerrainType.TriangleSW:                    
                    return (pointX * floorEntity.Height <= pointY * floorEntity.Width);

                default:
                    return true;
            }
        }
    }
}
