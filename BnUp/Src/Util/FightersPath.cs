﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Util
{
    class FightersPath
    {
        private const string Path = "./Content/Data/Fighters/";
        private const string Extension = "FighterData.json";
        private Dictionary<string, string> _pathMap = new Dictionary<string, string>();

        #region Singleton

        private static FightersPath _instance = null;

        public static FightersPath Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FightersPath();
                return _instance;
            }
        }

        private FightersPath()
        {
            InitMap();
        }
        #endregion

        private void InitMap()
        {
            AddFighter("Enemy");
            AddFighter("Boss");
        }

        private void AddFighter(string name)
        {
            _pathMap.Add(name, Path + name + Extension);
        }

        public string Get(string key)
        {
            return _pathMap[key];
        }
    }
}
