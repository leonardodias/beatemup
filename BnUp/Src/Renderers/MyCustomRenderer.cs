﻿using BnUp.Src.Components;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Renderers
{
    class MyCustomRenderer : Renderer
    {
        public BlendState BlendState;
        public SamplerState SamplerState;
        public Effect Effect;
        public Camera Camera;
        public Camera HudCamera;
        public bool DebugMode;

        private List<Entity> SortEntities = new List<Entity>();

        public MyCustomRenderer()
        {
            BlendState = BlendState.AlphaBlend;
            SamplerState = SamplerState.LinearClamp;
            Camera = new Camera();
            HudCamera = new Camera();
        }

        public override void BeforeRender(Scene scene)
        {
            SortEntities.Clear();

            foreach (var entity in scene.Entities)
            {
                if (entity.Visible && entity.TagCheck(EntityTags.zSortTag))
                    SortEntities.Add(entity);
            }

            SortEntities.Sort((a, b) =>
                a.Get<ZCoordComponent>().BaseCollider.Position.Y.CompareTo
                (b.Get<ZCoordComponent>().BaseCollider.Position.Y)
            );
        }

        public override void Render(Scene scene)
        {
            //
            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState, SamplerState, DepthStencilState.None, RasterizerState.CullNone, Effect, Camera.Matrix * Engine.ScreenMatrix);

            //draw background
            foreach (var entity in scene.Entities)
                if (entity.Visible && entity.TagCheck(EntityTags.backgroundTag))
                    entity.Render();

            //draw shadows
            foreach (var entity in scene.Entities)
                if (entity.Visible && entity.TagCheck(EntityTags.shadowTag))
                    entity.Render();

            //draw fighters
            foreach (var entity in SortEntities)
                if (entity.Visible)
                    entity.Render();

            //draw effects
            foreach (var entity in scene.Entities)
                if (entity.Visible && entity.TagCheck(EntityTags.effectTag))
                    entity.Render();

            //draw debug
            if (Engine.Commands.Open || DebugMode)
            {
                foreach (var entity in scene.Entities)
                    if (entity.Visible && !entity.TagCheck(EntityTags.hudElementsTag))
                        entity.DebugRender(Camera);
            }

            Draw.SpriteBatch.End();

            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState, SamplerState, DepthStencilState.None, RasterizerState.CullNone, Effect, HudCamera.Matrix * Engine.ScreenMatrix);

            scene.Entities.RenderOnly(EntityTags.hudElementsTag);

            Draw.SpriteBatch.End();
        }

        public override void AfterRender(Scene scene)
        {

        }
    }
}
