﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Tags
{
    class EntityTags
    {
        public static BitTag hudElementsTag = new BitTag("hudElements");
        public static BitTag playerTag = new BitTag("player");
        public static BitTag enemyTag = new BitTag("enemy");
        public static BitTag blockTag = new BitTag("block");
        public static BitTag fighterTag = new BitTag("fighter");
        public static BitTag cameraTag = new BitTag("camera");
        public static BitTag floorTag = new BitTag("floor");
        public static BitTag wallTag = new BitTag("wall");
        public static BitTag backgroundTag = new BitTag("background");
        public static BitTag attackTag = new BitTag("attack");
        public static BitTag effectTag = new BitTag("effect");
        public static BitTag shadowTag = new BitTag("shadow");
        public static BitTag zSortTag = new BitTag("zSort");
        public static BitTag breakableTag = new BitTag("breakable");
    }
}
