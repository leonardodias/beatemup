﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using BnUp.Src.Data;
using BnUp.Src.Entities;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Factory
{
    class StageFactory
    {
        public static Entity CreateStageBackground(BackgroundData backgroundData)
        {
            Entity backgroundEntity = new Entity();
            backgroundEntity.AddTag(EntityTags.backgroundTag);
            Image image = new Image(AssetsManager.TextureAtlas[backgroundData.ImageName]);
            backgroundEntity.Components.Add(image);
            backgroundEntity.Position.X = backgroundData.PosX;
            backgroundEntity.Position.Y = backgroundData.PosY;

            if (backgroundData.ParallaxDistance > 0)
                backgroundEntity.Add(new ParallaxComponent(backgroundData.ParallaxDistance));

            return backgroundEntity;
        }

        public static Entity CreateWall(TerrainData data)
        {
            TerrainType terrainType = TerrainType.Square;

            foreach (TerrainType type in Enum.GetValues(typeof(TerrainType)))
                if (type.ToString().Equals(data.TerrainType))
                    terrainType = type;

            return new WallEntity(
                data.Rectangle.X,
                data.Rectangle.Y,
                data.Rectangle.Width,
                data.Rectangle.Height,
                terrainType);
        }

        public static Entity CreateFloor(TerrainData data)
        {
            TerrainType terrainType = TerrainType.Square;

            foreach (TerrainType type in Enum.GetValues(typeof(TerrainType)))
                if (type.ToString().Equals(data.TerrainType))
                    terrainType = type;

            return new FloorEntity(
                data.Rectangle.X,
                data.Rectangle.Y,
                data.Rectangle.Width,
                data.Rectangle.Height,
                terrainType);
        }

        public static Entity CreateSection(SectionData sectionData)
        {
            Entity sectionEntity = new Entity();
            SectionControlComponent sectionControl = new SectionControlComponent(sectionData);
            sectionEntity.Add(sectionControl);
            return sectionEntity;
        }

        public static Entity CreateStageTriggerEntity(StageEntityData stageEntityData)
        {
            Entity createEntityData = new Entity();
            createEntityData.Collider = new Hitbox(
                stageEntityData.Trigger.Width,
                stageEntityData.Trigger.Height,
                stageEntityData.Trigger.X,
                stageEntityData.Trigger.Y);

            createEntityData.Add(new StageEntityComponent(stageEntityData));

            return createEntityData;
        }

        internal static Entity CreateBlock(BlockData blockData, int x, int y, int z)
        {
            int baseHeight = 12;

            Entity blockEntity = new Entity();
            blockEntity.Collider = new Hitbox(
               blockData.Width,
               blockData.Height);

            blockEntity.Position.X = x;
            blockEntity.Position.Y = y;

            blockEntity.AddTag(EntityTags.zSortTag);
            blockEntity.AddTag(EntityTags.blockTag);
            blockEntity.AddTag(EntityTags.breakableTag);

            blockEntity.Components.Add(new DirectionComponent());

            blockEntity.Components.Add(new GravityComponent());

            BreakableReceiveAttackComponent breakableReceiveAttackComponent = new BreakableReceiveAttackComponent();
            if (blockData.Animations != null && blockData.Animations.Count > 0)
            {
                breakableReceiveAttackComponent.Animations = blockData.Animations;
            }
            blockEntity.Components.Add(breakableReceiveAttackComponent);

            BreakableAttributesComponent breakableAttributesComponent = new BreakableAttributesComponent();
            breakableAttributesComponent.HP = blockData.HP;
            blockEntity.Components.Add(breakableAttributesComponent);

            blockEntity.Components.Add(new GroundCollsionComponent());

            blockEntity.Components.Add(new VelocityComponent());
            ZCoordComponent zCoord = new ZCoordComponent(blockData.Width, baseHeight);
            zCoord.BaseCollider.Position.X = x;
            zCoord.BaseCollider.Position.Y = y;
            zCoord.ZValue = z;
            blockEntity.Components.Add(zCoord);


            Sprite blockSprite = AssetsManager.AllSprites.Create(blockData.Sprite.Name);
            //playerSprite.X = -10;
            blockSprite.Y = blockData.Sprite.Y;
            blockEntity.Add(blockSprite);


            return blockEntity;
        }

        internal static Entity CreateBomb(BombData bombData, int x, int y, int z)
        {
            int baseHeight = 12;

            Entity bombEntity = new Entity();
            bombEntity.Collider = new Hitbox(
               25, 25);

            bombEntity.Position.X = x;
            bombEntity.Position.Y = y;

            bombEntity.AddTag(EntityTags.zSortTag);

            bombEntity.Add(new BombComponent());

            bombEntity.Components.Add(new DirectionComponent());

            bombEntity.Components.Add(new GravityComponent());

            bombEntity.Components.Add(new GroundCollsionComponent());

            bombEntity.Components.Add(new VelocityComponent());
            ZCoordComponent zCoord = new ZCoordComponent(25, baseHeight);
            zCoord.BaseCollider.Position.X = x;
            zCoord.BaseCollider.Position.Y = y;
            zCoord.ZValue = z;
            bombEntity.Components.Add(zCoord);


            Sprite bombSprite = AssetsManager.AllSprites.Create("bomba");
            //playerSprite.X = -10;
            bombEntity.Add(bombSprite);

            

            return bombEntity;
        }
    }
}
