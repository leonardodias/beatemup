﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using BnUp.Src.Data;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Factory
{
    class FighterFactory
    {
        public static Entity CreateFighter(FighterData fighterData, int x, int y, int z, int gamePadIndex = 0)
        {
            int baseHeight = 12;           

            Entity e = new Entity();

            e.AddTag(EntityTags.fighterTag);
            e.AddTag(EntityTags.zSortTag);

            if (fighterData.Player)
                e.AddTag(EntityTags.playerTag);
            else
                e.AddTag(EntityTags.enemyTag);

            e.Position.X = x;
            e.Position.Y = y;

            e.Collider = new Hitbox(fighterData.Width, fighterData.Height, 0, 0);

            if (fighterData.Player)
                e.Components.Add(new PlayerInputComponent(gamePadIndex));

            e.Components.Add(new DirectionComponent());

            e.Components.Add(new FighterFaintingComponent());

            e.Components.Add(new GravityComponent());

            if (fighterData.Player)
            {
                e.Components.Add(new PlayerMovementComponent());
                e.Components.Add(new PlayerJumpComponent());
                e.Components.Add(new PlayerAttackComponent());
                e.Components.Add(new PlayerDashComponent());
            }                
            else
                e.Components.Add(new BasicEnemyAIComponent());
            
            FighterMovementComponent fighterMovement = new FighterMovementComponent();
            fighterMovement.Velocity = fighterData.Velocity;
            e.Components.Add(fighterMovement);

            if (fighterData.Dash != null)
                e.Components.Add(new FighterDashComponent(fighterData.Dash));

            e.Components.Add(new FighterJumpComponent(fighterData.Jump));
       

            e.Components.Add(new FighterCameraKnockReboundComponent());

            e.Components.Add(new SmallJumpClimbDownWallComponent());

            e.Components.Add(new FighterReceiveAttackComponent());
            
            e.Components.Add(new FighterAttackComponent(fighterData));
            FighterAttributesComponent fighterAttributesComponent = new FighterAttributesComponent();
            fighterAttributesComponent.HP = fighterData.HP;
            fighterAttributesComponent.Unstoppable = fighterData.Unstoppable;
            e.Components.Add(fighterAttributesComponent);

            //if (fighterData.Player)
            //    e.Components.Add(new FighterGrabThrowComponent(fighterData));
            //else            
            //    e.Components.Add(new GrabbableComponent());

            if (fighterData.SpecialMove == "mic-hook")
            {
                e.Components.Add(new CreateMicHookComponent());
            }

            //e.Components.Add(new FallFromWallComponent());

            e.Components.Add(new BlockCollisionComponent());
            e.Components.Add(new WallCollisionComponent());
            e.Components.Add(new GroundCollsionComponent());

            e.Components.Add(new VelocityComponent());
            ZCoordComponent zCoord = new ZCoordComponent((int) e.Collider.Width, baseHeight);
            zCoord.BaseCollider.Position.X = x;
            zCoord.BaseCollider.Position.Y = y;
            zCoord.ZValue = z;
            e.Components.Add(zCoord);

            e.Components.Add(new FighterCameraLimitsComponent());

            FighterAnimationComponent fighterAnimationComponent = new FighterAnimationComponent();
            fighterAnimationComponent.LeftOffsetX = fighterData.Sprite.LeftOffsetX;
            fighterAnimationComponent.RightOffsetX = fighterData.Sprite.RightOffsetX;
            e.Components.Add(fighterAnimationComponent);

            Sprite playerSprite = AssetsManager.AllSprites.Create(fighterData.Sprite.Name);
            //playerSprite.X = -10;
            playerSprite.Y = fighterData.Sprite.Y;
            e.Add(playerSprite);            

            return e;
        }

        public static Entity CreateShadow(Entity fighterEntity, int shadowX = 0)
        {
            Entity shadowEntity = new Entity();
            shadowEntity.Collider = new Hitbox(36, 12);
            shadowEntity.AddTag(EntityTags.shadowTag);
            Image image = new Image(AssetsManager.TextureAtlas["sombra"]);            
            shadowEntity.Add(image);

            EntityPinComponent entityPin = new EntityPinComponent();
            entityPin.Target = fighterEntity;
            entityPin.SetRelativePosition(Enums.Direction.Left, (int) (fighterEntity.Width / 2 - 18) - shadowX, 0);
            entityPin.SetRelativePosition(Enums.Direction.Right, (int)(fighterEntity.Width / 2 - 18) + shadowX, 0);
            entityPin.FollowZBase = true;
            shadowEntity.Add(entityPin);

            shadowEntity.Add(new ShadowComponent(fighterEntity));

            return shadowEntity;
        }
    }
}
