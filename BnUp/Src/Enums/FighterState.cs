﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Enums
{
    enum FighterState
    {
        Standing,
        Hurt,
        KnockedOut,
        Attacking,
        Faint,
        Grabbed,
        GrabbedLifted,
        Grabbing,
        Rising,
        Throwing,
        Dashing
    }
}
