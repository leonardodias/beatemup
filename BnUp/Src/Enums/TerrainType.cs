﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Enums
{
    enum TerrainType
    {
        Square,
        TriangleNE,
        TriangleSE,
        TriangleNW,
        TriangleSW
    }
}
