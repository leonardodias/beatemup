﻿using BnUp.Src.Enums;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class PlayerAttackComponent : Component
    {
        private PlayerInputComponent _playerInputComponent;
        private FighterAttackComponent _fighterAttackComponent;
        private FighterGrabThrowComponent _fighterGrabThrowComponent;
        private FighterAttributesComponent _fighterAttributesComponent;

        public PlayerAttackComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _fighterAttackComponent = this.Entity.Components.Get<FighterAttackComponent>();
            _fighterGrabThrowComponent = this.Entity.Components.Get<FighterGrabThrowComponent>();
            _playerInputComponent = this.Entity.Components.Get<PlayerInputComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (!_playerInputComponent.LockControls && _playerInputComponent.AttackInput.Pressed)
            {
                if (_fighterAttributesComponent.State == Enums.FighterState.Grabbing
                    && MathF.Abs(_playerInputComponent.VirtualJoystick.Value.X) > 0.5f
                    )
                {                    
                    _fighterGrabThrowComponent.StartThrow(
                        _playerInputComponent.VirtualJoystick.Value.X > 0 ? Direction.Right : Direction.Left);
                }
                else
                    _fighterAttackComponent.StartAttack();
            }
        }


    }
}
