﻿using BnUp.Src.Entities;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using BnUp.Src.Util;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterReceiveAttackComponent : Component
    {
        private DirectionComponent _directionComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private Sprite _sprite;

        private float currentKnockedFly;
        private float knockedFly = 0.1f;

        private int knockedHitGround = 0;
        private float currentKnockOutRecoveryTime;
        private float knockOutRecoveryTime = 0.8f;

        private float currentHurtRecoveryTime = 0.6f;
        private float hurtRecoveryTime = 0.6f;

        private float currentRisingRecoveryTime = 0.2f;
        private float risingRecoveryTime = 0.2f;


        public FighterReceiveAttackComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();

            base.EntityAwake();
        }

        public override void Update()
        {
            switch (_fighterAttributesComponent.State)
            {
                case FighterState.KnockedOut:

                    switch (knockedHitGround)
                    {
                        case 0:
                            currentKnockedFly -= Engine.DeltaTime;

                            if (currentKnockedFly < 0 && _groundCollsionComponent.OnGround)
                            {
                                _zCoordComponent.ZVelocity = -120;
                                currentKnockedFly = knockedFly;
                                knockedHitGround++;

                                _velocityComponent.SetVelocityX(_velocityComponent.Velocity.X / 2);
                            }

                            break;
                        case 1:
                            currentKnockedFly -= Engine.DeltaTime;

                            if (currentKnockedFly < 0 && _groundCollsionComponent.OnGround)
                            {
                                knockedHitGround++;
                                _velocityComponent.SetVelocityX(0);
                            }

                            break;
                        case 2:
                            currentKnockOutRecoveryTime -= Engine.DeltaTime;

                            if (currentKnockOutRecoveryTime <= 0)
                            {
                                if (_fighterAttributesComponent.HP <= 0)
                                    _fighterAttributesComponent.State = FighterState.Faint;
                                else
                                    _fighterAttributesComponent.State = FighterState.Rising;
                            }

                            break;
                    }

                    break;
                case FighterState.Hurt:
                    if (currentHurtRecoveryTime <= 0)
                    {
                        _fighterAttributesComponent.State = FighterState.Standing;
                        currentHurtRecoveryTime = hurtRecoveryTime;
                    }
                    else
                    {
                        currentHurtRecoveryTime -= Engine.DeltaTime;
                    }

                    break;
                case FighterState.Rising:
                    if (currentRisingRecoveryTime <= 0)
                    {
                        _fighterAttributesComponent.State = FighterState.Standing;
                        currentRisingRecoveryTime = risingRecoveryTime;
                    }
                    else
                    {
                        currentRisingRecoveryTime -= Engine.DeltaTime;
                    }

                    break;
            }

        }


        internal void GetHit(int damage, bool knockHit, Direction direction = Direction.Right, int xVelocity = 200, int zVelocity = -200)
        {
            if (_fighterAttributesComponent.State != FighterState.Faint
                && _fighterAttributesComponent.State != FighterState.KnockedOut
                && _fighterAttributesComponent.State != FighterState.Rising)
            {
                _fighterAttributesComponent.HP -= damage;

                if (_fighterAttributesComponent.HP <= 0)
                {
                    _fighterAttributesComponent.HP = 0;
                    knockHit = true;
                    _fighterAttributesComponent.State = FighterState.Faint;
                }

                if (!_groundCollsionComponent.OnGround)
                {
                    knockHit = true;
                }

                if (knockHit)
                {
                    _zCoordComponent.ZVelocity = zVelocity;
                    _directionComponent.Direction = DirectionExtension.Opposite(direction);

                    SetKnockedOut();

                    switch (direction)
                    {
                        case Direction.Left:
                            _velocityComponent.SetVelocityX(-xVelocity);
                            break;
                        case Direction.Right:
                            _velocityComponent.SetVelocityX(xVelocity);
                            break;
                    }

                    _velocityComponent.SetVelocityY(0);
                }
                else
                {
                    if (_fighterAttributesComponent.State != FighterState.Grabbed
                        && _fighterAttributesComponent.State != FighterState.GrabbedLifted
                        && !_fighterAttributesComponent.Unstoppable)
                        _fighterAttributesComponent.State = FighterState.Hurt;
                    currentHurtRecoveryTime = hurtRecoveryTime;

                    if (_groundCollsionComponent.OnGround)
                    {
                        _velocityComponent.SetVelocity(0, 0);
                    }
                }
            }
        }

        private void SetKnockedOut()
        {
            _fighterAttributesComponent.State = FighterState.KnockedOut;
            knockedHitGround = 0;
            currentKnockedFly = knockedFly;
            currentKnockOutRecoveryTime = knockOutRecoveryTime;
        }
    }
}
