﻿using BnUp.Src.Assets;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using MonoBMFontUWP.Loader;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class OpenSceneEffectComponent : Component
    {
        private float showTitleTime = 1f;

        private const float TotalOpenSceneTime = 1f;
        private float openSceneTime = 1f;

        private int state = 0;

        private BmFont Font { get; set; }

        private const string Title = "Stage 1 Start";

        private List<PlayerInputComponent> playerInputs = new List<PlayerInputComponent>();

        public OpenSceneEffectComponent() : base(true, true)
        {
            Font = AssetsManager.GetFont(Enums.FontKey.ZCool24);
        }

        public override void EntityAwake()
        {
            openSceneTime = TotalOpenSceneTime;

            foreach (var item in Scene[EntityTags.playerTag])
            {
                PlayerInputComponent playerInput = item.Get<PlayerInputComponent>();
                if (playerInput != null)
                    playerInputs.Add(playerInput);
            }

            base.EntityAwake();
        }

        public override void Update()
        {
            switch (state)
            {
                case 0:
                    showTitleTime -= Engine.DeltaTime;

                    if (showTitleTime <= 0)
                        state = 1;

                    foreach (var item in playerInputs)                    
                        item.LockControls = true;                    

                    break;
                case 1:
                    openSceneTime -= Engine.DeltaTime;
                    if (openSceneTime < 0)
                    {
                        foreach (var item in playerInputs)                        
                            item.LockControls = false;                       

                        this.Entity.RemoveSelf();
                    }

                    break;
            }

            base.Update();
        }

        public override void Render()
        {
            float opening = (Engine.Height / 2) * (openSceneTime / TotalOpenSceneTime);

            Draw.Rect(0, 0, Engine.Width, opening, Color.Black);
            Draw.Rect(0, Engine.Height - opening, Engine.Width, opening, Color.Black);

            int textWidth = Font.GetWidth(Title);
            Font.Draw(Title, (int)(Engine.Width / 2 - textWidth / 2),
                Engine.Height / 2 - 12, Draw.SpriteBatch);

            base.Render();
        }
    }
}
