﻿using BnUp.Src.Enums;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class BreakableAttributesComponent : Component
    {
        public string Name { get; set; }
        public int HP { get; set; }

        public bool Broken { get; set; }

        public BreakableAttributesComponent() : base(false, false)
        {
            
        }
    }
}