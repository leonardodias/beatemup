﻿using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FallFromWallComponent : Component
    {
        private VelocityComponent _velocityComponent;
        private ZCoordComponent _zCoordComponent;
        private Hitbox testFindGroundHitbox;
        private FighterAttributesComponent _fighterAttributesComponent;

        public FallFromWallComponent() : base(true, false)
        {
            testFindGroundHitbox = new Hitbox(0, 0);

        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            base.EntityAwake();
        }

        public override void Update()
        {
            if (_velocityComponent != null && _zCoordComponent != null
                && _fighterAttributesComponent != null)
            {
                if (_zCoordComponent.ZValue >= 0 
                    && _fighterAttributesComponent.State != Enums.FighterState.Grabbed
                    && _fighterAttributesComponent.State != Enums.FighterState.GrabbedLifted
                    && _fighterAttributesComponent.State != Enums.FighterState.Grabbing)
                {
                    foreach (var wallEntity in Scene[EntityTags.wallTag])
                    {
                        if (!wallEntity.Equals(this.Entity) && this.Entity.CollideCheck(wallEntity))
                        {
                            _zCoordComponent.ZValue = ((int)(
                                _zCoordComponent.BaseCollider.Position.Y + _zCoordComponent.BaseCollider.Height
                                - (wallEntity.Position.Y + wallEntity.Height)));

                            RepositionInGroundBelow();

                        }
                    }
                }
            }
        }

        private void RepositionInGroundBelow()
        {
            testFindGroundHitbox.Set(
                _zCoordComponent.BaseCollider.Position.X,
                _zCoordComponent.BaseCollider.Position.Y + _zCoordComponent.BaseCollider.Height,
                _zCoordComponent.BaseCollider.Width,
                5000);

            foreach (var floorEntity in Scene[EntityTags.floorTag])
            {
                if (testFindGroundHitbox.Collide(floorEntity))
                {
                    _zCoordComponent.BaseCollider.Position.Y = floorEntity.Y + 1;
                }
            }
        }
    }
}
