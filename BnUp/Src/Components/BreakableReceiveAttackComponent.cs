﻿using BnUp.Src.Data;
using BnUp.Src.Entities;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using BnUp.Src.Util;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class BreakableReceiveAttackComponent : Component
    {
        private DirectionComponent _directionComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private BreakableAttributesComponent _breakableAttributesComponent;
        private Sprite _sprite;

        private float brokeAnimationDelay = 0.5f;

        public List<BreakableAnimationData> Animations { get; set; }
        public int animationIndex;

        public BreakableReceiveAttackComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _breakableAttributesComponent = this.Entity.Components.Get<BreakableAttributesComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();

            if (Animations != null)
            {
                Animations.Sort((a, b) => b.HP.CompareTo(a.HP));
            }

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_breakableAttributesComponent.Broken)
            {
                brokeAnimationDelay -= Engine.DeltaTime;

                if (brokeAnimationDelay <= 0
                    || (_groundCollsionComponent.OnGround
                    && _zCoordComponent.ZVelocity > 0)
                    )
                {
                    this.Entity.RemoveSelf();
                }
            }

            UpdateAnimations();
        }

        private void UpdateAnimations()
        {
            if (Animations != null && Animations.Count > 0)
            {
                if (animationIndex < Animations.Count - 1 && 
                    _breakableAttributesComponent.HP < Animations[animationIndex].HP)
                {
                    animationIndex++;
                    _sprite.Play(Animations[animationIndex].Name);
                }
            }
        }

        internal void GetHit(int damage, bool knockHit, Direction direction = Direction.Right, int xVelocity = 200, int zVelocity = -200)
        {
            if (!_breakableAttributesComponent.Broken)
            {
                _breakableAttributesComponent.HP -= damage;

                if (_breakableAttributesComponent.HP <= 0)
                {
                    _breakableAttributesComponent.HP = 0;
                    knockHit = true;
                    _breakableAttributesComponent.Broken = true;
                }

                if (!_groundCollsionComponent.OnGround)
                {
                    knockHit = true;
                }

                if (knockHit)
                {
                    _zCoordComponent.ZVelocity = zVelocity;
                    _directionComponent.Direction = DirectionExtension.Opposite(direction);

                    switch (direction)
                    {
                        case Direction.Left:
                            _velocityComponent.SetVelocityX(-xVelocity);
                            break;
                        case Direction.Right:
                            _velocityComponent.SetVelocityX(xVelocity);
                            break;
                    }

                    _velocityComponent.SetVelocityY(0);
                }
            }
        }

    }
}
