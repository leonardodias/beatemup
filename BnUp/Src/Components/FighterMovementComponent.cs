﻿using BnUp.Src.Enums;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterMovementComponent : Component
    {
        public float MovementX { get; set; }
        public float MovementY { get; set; }

        public float Velocity { get; set; }

        public bool OnAirMovement { get; set; }

        public bool LockDirection { get; set; }

        private DirectionComponent _directionComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private FighterJumpComponent _fighterJumpComponent;

        public FighterMovementComponent() : base(true, false)
        {


        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _fighterJumpComponent = this.Entity.Components.Get<FighterJumpComponent>();

            base.EntityAwake();
        }


        public override void Update()
        {
            if (_fighterAttributesComponent.State == Enums.FighterState.Standing)
            {
                bool allowMovement = (OnAirMovement ||                     
                    (_groundCollsionComponent.OnGround
                    && _zCoordComponent.ZVelocity >= 0));
                bool startJumping = _fighterJumpComponent != null && _fighterJumpComponent.IsStartingJump();

                if (allowMovement && !startJumping)
                {
                    _velocityComponent.SetVelocity(
                    MovementX * Velocity,
                    MovementY * Velocity);
                }

                if (!LockDirection)
                    UpdateDirection();
            }
        }

        internal void SetMovement(float x, float y)
        {
            MovementX = x;
            MovementY = y;
        }

        private void UpdateDirection()
        {
            if (MovementX < 0)
            {
                _directionComponent.Direction = Enums.Direction.Left;
            }
            else if (MovementX > 0)
            {
                _directionComponent.Direction = Enums.Direction.Right;
            }
        }
    }
}
