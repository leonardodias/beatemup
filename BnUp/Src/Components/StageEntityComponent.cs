﻿using BnUp.Src.Data;
using BnUp.Src.Factory;
using BnUp.Src.Tags;
using BnUp.Src.Util;
using Microsoft.Xna.Framework;
using Monocle;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class StageEntityComponent : Component
    {
        private Entity camera;
        private Rectangle TriggerArea { get; set; }
        private string entityFile;
        private PointData spawnPosition;
        private string entityType;

        public StageEntityComponent(StageEntityData stageEntityData) : base(true, false)
        {
            TriggerArea = new Rectangle(
               stageEntityData.Trigger.X,
               stageEntityData.Trigger.Y,
               stageEntityData.Trigger.Width,
               stageEntityData.Trigger.Height);

            entityFile = stageEntityData.EntityFile;
            spawnPosition = stageEntityData.Position;
            entityType = stageEntityData.EntityType;
        }

        public override void Update()
        {
            if (camera == null)
                camera = Scene[EntityTags.cameraTag][0];

            if (TriggerArea.Contains(camera.Get<CameraMovementComponent>().GetCameraCenter()))
            {
                CreateEntity();
                this.Entity.RemoveSelf();
            }            
        }

        private void CreateEntity()
        {
            switch (entityType)
            {
                case "enemy":
                    CreateEnemy();

                    break;
                case "block":
                    CreateBlock();

                    break;
                case "bomb":
                    CreateBomb();

                    break;
            }

        }

        private void CreateEnemy()
        {
            string filePath = FightersPath.Instance.Get(entityFile);
            FighterData testDataTwo =
                JObject.Parse(System.IO.File.ReadAllText(filePath)).ToObject<FighterData>();

            Entity enemyEntity = FighterFactory.CreateFighter(testDataTwo,
                spawnPosition.X,
                spawnPosition.Y,
                spawnPosition.Z);
            Scene.Add(enemyEntity);

            Scene.Add(FighterFactory.CreateShadow(enemyEntity));
        }

        private void CreateBlock()
        {
            string filePath = BlocksPath.Instance.Get(entityFile);
            BlockData testDataTwo =
                JObject.Parse(System.IO.File.ReadAllText(filePath)).ToObject<BlockData>();

            Entity blockEntity = StageFactory.CreateBlock(testDataTwo,
                spawnPosition.X,
                spawnPosition.Y,
                spawnPosition.Z);

            Scene.Add(blockEntity);            
        }


        private void CreateBomb()
        {
            //string filePath = BlocksPath.Instance.Get(entityFile);
            //BombData testDataTwo =
            //    JObject.Parse(System.IO.File.ReadAllText(filePath)).ToObject<BombData>();

            Entity bombEntity = StageFactory.CreateBomb(new BombData(),
                spawnPosition.X,
                spawnPosition.Y,
                spawnPosition.Z);

            Scene.Add(bombEntity);
        }

    }
}
