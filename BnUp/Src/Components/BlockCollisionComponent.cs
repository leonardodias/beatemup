﻿using BnUp.Src.Tags;
using BnUp.Src.Util;
using Monocle;
using System;

namespace BnUp.Src.Components
{
    class BlockCollisionComponent : Component
    {
        private VelocityComponent _velocityComponent;
        private ZCoordComponent _zCoordComponent;

        private Hitbox testHitBox = new Hitbox(0, 0, 0, 0);

        public BlockCollisionComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            base.EntityAwake();
        }

        public override void Update()
        {
            if (_velocityComponent != null && _zCoordComponent != null)
            {
                foreach (var otherEntity in Scene[EntityTags.blockTag])
                {
                    if (!otherEntity.Equals(this.Entity))
                    {
                        HorizontalCheck(_velocityComponent, _zCoordComponent, otherEntity, _velocityComponent.Velocity.X);
                        VerticalCheck(_velocityComponent, _zCoordComponent, otherEntity, _velocityComponent.Velocity.Y);
                    }
                }
            }
        }

        private float HorizontalCheck(VelocityComponent velocityComponent, ZCoordComponent zCoordComponent, Entity blockEntity, float targetX)
        {
            WallDirectionComponent wallDirectionComponent = blockEntity.Get<WallDirectionComponent>();

            if (wallDirectionComponent != null)
            {
                if (!wallDirectionComponent.BlockRight && velocityComponent.Velocity.X > 0)
                {
                    return targetX;
                }

                if (!wallDirectionComponent.BlockLeft && velocityComponent.Velocity.X < 0)
                {
                    return targetX;
                }
            }

            testHitBox.Set(
                _zCoordComponent.BaseCollider.Position.X,
                _zCoordComponent.BaseCollider.Position.Y,
                _zCoordComponent.BaseCollider.Width,
                _zCoordComponent.BaseCollider.Height);
            float positionVariation = ((float)Math.Round(targetX * Engine.DeltaTime));
            testHitBox.Position.X += positionVariation;

            bool collision = false;

            if (blockEntity.Get<ZCoordComponent>() != null)
                collision = testHitBox.Collide(blockEntity.Get<ZCoordComponent>().BaseCollider);
            else
                collision = testHitBox.Collide(blockEntity);

            if (collision)
            {
                TerrainTypeComponent floorComponent = blockEntity.Get<TerrainTypeComponent>();

                if (floorComponent != null)
                    collision =
                        TerrainTypeUtil.CheckFloorType(testHitBox, blockEntity, floorComponent.FloorType);
            }

            if (collision)
            {
                if (Math.Abs(positionVariation) <= 1)
                {
                    targetX = 0;
                }
                else
                {
                    targetX = HorizontalCheck(velocityComponent, zCoordComponent, blockEntity, targetX / 2);
                }
            }

            if (Math.Abs(targetX) < Math.Abs(velocityComponent.Velocity.X))
            {
                velocityComponent.SetVelocityX(targetX);
            }

            return targetX;
        }

        private float VerticalCheck(VelocityComponent velocityComponent, ZCoordComponent zCoordComponent, Entity blockEntity, float targetY)
        {
            WallDirectionComponent wallDirectionComponent = blockEntity.Get<WallDirectionComponent>();

            if (wallDirectionComponent != null)
            {

                if (!wallDirectionComponent.BlockUp && velocityComponent.Velocity.Y < 0)
                {
                    return targetY;
                }

                if (!wallDirectionComponent.BlockDown && velocityComponent.Velocity.Y > 0)
                {
                    return targetY;
                }
            }

            testHitBox.Set(
                _zCoordComponent.BaseCollider.Position.X,
                _zCoordComponent.BaseCollider.Position.Y,
                _zCoordComponent.BaseCollider.Width,
                _zCoordComponent.BaseCollider.Height);

            float positionVariation = ((float)Math.Round(targetY * Engine.DeltaTime));
            testHitBox.Position.Y += positionVariation;

            bool collision = false;

            if (blockEntity.Get<ZCoordComponent>() != null)
                collision = testHitBox.Collide(blockEntity.Get<ZCoordComponent>().BaseCollider);
            else
                collision = testHitBox.Collide(blockEntity);

            if (collision)
            {
                TerrainTypeComponent floorComponent = blockEntity.Get<TerrainTypeComponent>();

                if (floorComponent != null)
                    collision =
                        TerrainTypeUtil.CheckFloorType(testHitBox, blockEntity, floorComponent.FloorType);
            }

            if (collision)
            {
                if (Math.Abs(positionVariation) <= 1)
                {
                    targetY = 0;
                }
                else
                {
                    targetY = VerticalCheck(velocityComponent, zCoordComponent, blockEntity, targetY / 2);
                }
            }

            if (Math.Abs(targetY) < Math.Abs(velocityComponent.Velocity.Y))
            {
                velocityComponent.SetVelocityY(targetY);
            }

            return targetY;
        }
    }
}
