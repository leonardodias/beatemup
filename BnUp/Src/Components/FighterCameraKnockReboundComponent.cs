﻿using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterCameraKnockReboundComponent : Component
    {
        private bool _enabled = false;
        private CameraMovementComponent _cameraMovementComponent;
        private Rectangle entityRectangle = new Rectangle();

        private ZCoordComponent _zCoordComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private VelocityComponent _velocityComponent;
        private DirectionComponent _directionComponent;
        private Hitbox testHitBox = new Hitbox(0, 0);

        private float maxZVelocity = 200;

        public FighterCameraKnockReboundComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _enabled = false;
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_cameraMovementComponent == null)
                _cameraMovementComponent = Scene[EntityTags.cameraTag][0].Get<CameraMovementComponent>();
            else
            {
                if (_enabled)
                {
                    if (_fighterAttributesComponent.State == Enums.FighterState.KnockedOut)
                    {
                        HorizontalCheck();
                    }
                }
                else
                {
                    entityRectangle.X = (int)this.Entity.Position.X;
                    entityRectangle.Y = (int)this.Entity.Position.Y;
                    entityRectangle.Width = (int)this.Entity.Width;
                    entityRectangle.Height = (int)this.Entity.Height;

                    if (_cameraMovementComponent.GetCameraBounds().Contains(entityRectangle))
                    {
                        _enabled = true;
                    }
                }
            }

            base.Update();
        }

        private void HorizontalCheck()
        {       

            testHitBox.Set(
                _zCoordComponent.BaseCollider.Position.X,
                _zCoordComponent.BaseCollider.Position.Y,
                _zCoordComponent.BaseCollider.Width,
                _zCoordComponent.BaseCollider.Height);
            float positionVariation = ((float)Math.Round(_velocityComponent.Velocity.X * Engine.DeltaTime));
            testHitBox.Position.X += positionVariation;            

            if ((positionVariation > 0 &&
                testHitBox.TopRight.X > _cameraMovementComponent.GetCameraBounds().X + _cameraMovementComponent.GetCameraBounds().Width)
                ||
                (positionVariation < 0 &&
                testHitBox.TopLeft.X < _cameraMovementComponent.GetCameraBounds().X)

                )
            {
                _velocityComponent.SetVelocityX(_velocityComponent.Velocity.X * (-1));
                _zCoordComponent.ZVelocity -= MathF.Min(maxZVelocity, MathF.Abs(_velocityComponent.Velocity.X));
                _directionComponent.TurnDirection();

            }   
        }
    }
}