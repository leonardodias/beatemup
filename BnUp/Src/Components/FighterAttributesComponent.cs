﻿using BnUp.Src.Enums;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterAttributesComponent : Component
    {
        public string Name { get; set; }
        public int HP { get; set; }
        public bool Unstoppable { get; set; }

        public FighterState State { get; set; }
        
        public FighterAttributesComponent() : base(false, false)
        {
            State = FighterState.Standing;
        }
    }
}
