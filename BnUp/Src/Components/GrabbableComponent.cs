﻿using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class GrabbableComponent : Component
    {
        private DirectionComponent _directionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private ZCoordComponent _zCoordComponent;

        public Hitbox FrontGrab { get; private set; }
        public Hitbox RearGrab { get; private set; }

        public Entity GrabbingEntity { get; set; }

        private float _releaseGrabTime = 2f;
        private float _currentReleaseGrabTime = 2f;

        public GrabbableComponent() : base(true, false)
        {
            FrontGrab = new Hitbox(8, 8);
            RearGrab = new Hitbox(8, 8);
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_directionComponent.Direction == Enums.Direction.Left)
            {
                FrontGrab.Position.X = this.Entity.Position.X;
                FrontGrab.Position.Y = this.Entity.CenterY - 1;

                RearGrab.Position.X = this.Entity.Position.X + this.Entity.Width - RearGrab.Width;
                RearGrab.Position.Y = this.Entity.CenterY - 1;
            }
            else
            {
                FrontGrab.Position.X = this.Entity.Position.X + this.Entity.Width - RearGrab.Width;
                FrontGrab.Position.Y = this.Entity.CenterY - 1;

                RearGrab.Position.X = this.Entity.Position.X;
                RearGrab.Position.Y = this.Entity.CenterY - 1;
            }

            if ((_fighterAttributesComponent.State == Enums.FighterState.Grabbed
                || _fighterAttributesComponent.State == Enums.FighterState.GrabbedLifted))
            {
                if (GrabbingEntity != null)
                {
                    if (GrabbingEntity.Get<FighterAttributesComponent>().State == Enums.FighterState.Grabbing
                        || GrabbingEntity.Get<FighterAttributesComponent>().State == Enums.FighterState.Attacking
                        || GrabbingEntity.Get<FighterAttributesComponent>().State == Enums.FighterState.Throwing)
                    {
                        _zCoordComponent.BaseCollider.Position.Y = GrabbingEntity.Position.Y - 1;

                        //Solucao tosca para problema de "cair do cenario"
                        foreach (var wall in Scene[EntityTags.wallTag])                        
                            if (_zCoordComponent.BaseCollider.Collide(wall.Collider))
                            {
                                _zCoordComponent.BaseCollider.Position.Y += 1;
                                GrabbingEntity.Position.Y += 1;
                                break;
                            }

                        _zCoordComponent.ZValue = GrabbingEntity.Get<ZCoordComponent>().ZValue;
                        _zCoordComponent.ZVelocity = 0;
                    }
                    else
                    {
                        _fighterAttributesComponent.State = Enums.FighterState.Standing;
                    }
                }
                else
                {
                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                }

                _currentReleaseGrabTime -= Engine.DeltaTime;

                if (_currentReleaseGrabTime <= 0)
                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
            }
            else
            {
                GrabbingEntity = null;

                if (_fighterAttributesComponent.State != Enums.FighterState.Hurt)
                    _currentReleaseGrabTime = _releaseGrabTime;
            }
        }
    }
}

