﻿using BnUp.Src.Assets;
using BnUp.Src.Controllers;
using BnUp.Src.Entities;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class AttackComponent : Component
    {
        private DirectionComponent _directionComponent;
        private FighterAttackComponent _ownerFigherAttackComponent;
        private FighterAttributesComponent _ownerFigherAttributesComponent;

        private bool _enabled = true;

        private Entity owner;



        public Entity Owner
        {
            get { return owner; }
            set
            {
                owner = value;
                _ownerFigherAttackComponent = owner.Get<FighterAttackComponent>();
                _ownerFigherAttributesComponent = owner.Get<FighterAttributesComponent>();
            }
        }

        public bool KnockHit { get; set; }
        public int Damage { get; set; }

        public Hitbox AttackCollider;
        private Hitbox testBaseHitbox;
        private Hitbox testAttackHitbox;

        public AttackComponent() : base(true, true)
        {
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _enabled = true;
            testBaseHitbox = new Hitbox(this.Entity.Width, this.Entity.Height);
            testAttackHitbox = new Hitbox(AttackCollider.Width, AttackCollider.Height);

            base.EntityAwake();
        }

        public override void Update()
        {
            if (Owner == null || _ownerFigherAttributesComponent == null
                || _ownerFigherAttributesComponent.State != Enums.FighterState.Attacking)
            {
                _enabled = false;
            }

            if (_enabled)
            {
                testBaseHitbox.Position.X = this.Entity.Position.X;
                testBaseHitbox.Position.Y = this.Entity.Position.Y;

                if (_directionComponent.Direction == Enums.Direction.Right)
                {
                    testAttackHitbox.Position.X = Owner.Position.X + AttackCollider.Position.X;
                    testAttackHitbox.Position.X += Owner.Width;
                }
                else
                {
                    testAttackHitbox.Position.X = Owner.Position.X - AttackCollider.Position.X;
                    testAttackHitbox.Position.X -= testAttackHitbox.Width;
                }

                testAttackHitbox.Position.Y = Owner.Position.Y + AttackCollider.Position.Y;

                foreach (var otherEntity in Scene[EntityTags.fighterTag])
                {
                    if (!otherEntity.Equals(this.Entity) && !otherEntity.Equals(Owner))
                    {
                        FighterAttributesComponent targetAttributes = otherEntity.Get<FighterAttributesComponent>();

                        if (targetAttributes.State != Enums.FighterState.Faint
                            && targetAttributes.State != Enums.FighterState.KnockedOut
                            && targetAttributes.State != Enums.FighterState.Rising)
                        {
                            CheckAttack(otherEntity);
                        }
                    }
                }

                foreach (var otherEntity in Scene[EntityTags.breakableTag])
                {
                    if (!otherEntity.Equals(this.Entity) && !otherEntity.Equals(Owner))
                    {
                        BreakableAttributesComponent targetAttributes = otherEntity.Get<BreakableAttributesComponent>();

                        if (targetAttributes != null)
                        {
                            CheckAttack(otherEntity);
                        }
                    }
                }
            }
        }

        private void CheckAttack(Entity otherEntity)
        {
            bool collideBase = testBaseHitbox.Collide(otherEntity.Get<ZCoordComponent>().BaseCollider);

            bool collideAttack =
                testAttackHitbox.Collide(otherEntity.Collider);

            if (collideBase && collideAttack)
            {
                _enabled = false;

                _ownerFigherAttackComponent.AddComboCount();

                //effects
                CreateImpactEffects(otherEntity);

                if (otherEntity.TagCheck(EntityTags.fighterTag))
                {
                    FighterReceiveAttackComponent receiveAttackComponent =
                    otherEntity.Components.Get<FighterReceiveAttackComponent>();

                    if (receiveAttackComponent != null)
                    {
                        receiveAttackComponent.GetHit
                            (Damage,
                            KnockHit,
                            _directionComponent.Direction);
                    }
                }

                if (otherEntity.TagCheck(EntityTags.breakableTag))
                {
                    BreakableReceiveAttackComponent receiveAttackComponent =
                    otherEntity.Components.Get<BreakableReceiveAttackComponent>();

                    if (receiveAttackComponent != null)
                    {
                        receiveAttackComponent.GetHit
                            (Damage,
                            KnockHit,
                            _directionComponent.Direction);
                    }
                }
            }
        }

        private void CreateImpactEffects(Entity otherEntity)
        {
            SoundController.Instance.PlaySound(Enums.SoundKey.PunchImpact);
            

            float impactCenterX =
                MathF.Max(this.testAttackHitbox.Position.X,
                otherEntity.Position.X) +
                ((MathF.Min(this.testAttackHitbox.CenterRight.X,
                otherEntity.CenterRight.X) -
                MathF.Max(this.testAttackHitbox.Position.X,
                otherEntity.Position.X)) / 2);

            float impactCenterY =
                 MathF.Max(this.testAttackHitbox.TopCenter.Y,
                otherEntity.TopCenter.Y)
                +
                (
                (MathF.Min(this.testAttackHitbox.BottomCenter.Y,
                otherEntity.BottomCenter.Y) -
                MathF.Max(this.testAttackHitbox.TopCenter.Y,
                otherEntity.TopCenter.Y)) / 2);

            ImpactEntity impactEntity = Engine.Pooler.Create<ImpactEntity>();

            if (_directionComponent.Direction == Enums.Direction.Left)
            {
                impactEntity.Position.X = impactCenterX - impactEntity.Width / 2 + 16;
                impactEntity.Position.Y = impactCenterY - impactEntity.Height / 2;

                impactEntity.Get<Sprite>().FlipX = true;
            }
            else
            {
                impactEntity.Position.X = impactCenterX - impactEntity.Width / 2 - 16;
                impactEntity.Position.Y = impactCenterY - impactEntity.Height / 2;

                impactEntity.Get<Sprite>().FlipX = false;
            }

            Scene.Add(impactEntity);

            if (KnockHit)
            {
                Engine.FreezeTimer = 0.1f;

                foreach (var c in Scene[EntityTags.cameraTag])
                {
                    if (c.Get<Shaker>() != null)
                    {
                        c.Get<Shaker>().ShakeFor(0.2f, false);
                    }
                }
            }
        }

        public override void DebugRender(Camera camera)
        {
            base.DebugRender(camera);
            testAttackHitbox.Render(camera, Color.Green);
        }

        public override void Render()
        {
            base.Render();
        }
    }
}
