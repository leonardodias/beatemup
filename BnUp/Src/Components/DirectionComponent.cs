﻿
using BnUp.Src.Enums;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class DirectionComponent : Component
    {
        public Direction Direction { get; set; }       

        public DirectionComponent() : base(false, false)
        {
            Direction = Direction.Right;
        }

        internal void TurnDirection()
        {
            if (Direction == Direction.Left)
                Direction = Direction.Right;
            else
                Direction = Direction.Left;
        }
    }
}
