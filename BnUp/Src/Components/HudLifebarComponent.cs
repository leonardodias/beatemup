﻿using BnUp.Src.Assets;
using BnUp.Src.Enums;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class HudLifebarComponent : Component
    {
        private Entity _fighter;
        private FighterAttributesComponent _fighterAttributesComponent;

        private int _gamePadIndex;

        Image lifeBarYellow1;
        Image lifeBarYellow2;
        Image lifeBarRed1;
        Image lifeBarRed2;
        Image lifeBarCorner;

        private Vector2 lifebarPosition = new Vector2(10, 10);

        public HudLifebarComponent(int gamePadIndex, Entity fighter) : base(true, true)
        {
            _gamePadIndex = gamePadIndex;
            _fighter = fighter;

            lifeBarYellow1 = new Image(AssetsManager.TextureAtlas["hud/lifebaryellow1"]);
            lifeBarYellow2 = new Image(AssetsManager.TextureAtlas["hud/lifebaryellow2"]);
            lifeBarRed1 = new Image(AssetsManager.TextureAtlas["hud/lifebarred1"]);
            lifeBarRed2 = new Image(AssetsManager.TextureAtlas["hud/lifebarred2"]);
            lifeBarCorner = new Image(AssetsManager.TextureAtlas["hud/lifebar-corner"]);
        }

        public override void EntityAwake()
        {
            _fighterAttributesComponent = _fighter.Get<FighterAttributesComponent>();

            base.EntityAwake();
        }

        public override void Render()
        {
            float startX = lifebarPosition.X;

            lifeBarCorner.Texture.Draw(lifebarPosition);
            lifebarPosition.X += 1;

            for (int i = 0; i < 100; i++)
            {
                if (i < _fighterAttributesComponent.HP)
                    lifeBarYellow1.Texture.Draw(lifebarPosition);
                else
                    lifeBarRed1.Texture.Draw(lifebarPosition);

                lifebarPosition.X += 1; 
            }

            if (100 == _fighterAttributesComponent.HP)
                lifeBarYellow2.Texture.Draw(lifebarPosition);
            else
                lifeBarRed2.Texture.Draw(lifebarPosition);

            lifebarPosition.X += 1;
            lifeBarCorner.Texture.Draw(lifebarPosition);

            lifebarPosition.X = startX;
        }
    }
}
