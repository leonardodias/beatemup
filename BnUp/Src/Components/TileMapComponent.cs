﻿using Microsoft.Xna.Framework;
using Monocle;
using BnUp.Src.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class TileMapComponent : Component
    {
        TileMap _tileMap;
        private Rectangle translatedRectangle = new Rectangle();
        private Rectangle sourceRectangle = new Rectangle();

        private Vector2 _tileScale = new Vector2(0.01f, 0.01f);

        public TileMapComponent(TileMap tileMap) : base(true, true)
        {
            _tileMap = tileMap;
        }

        public override void EntityAwake()
        {
            
        }

        public override void Update()
        {
                       
        }

        public override void Render()
        {
            foreach (var tileMapLayer in _tileMap.Layers)
            {
                RenderTileLayer(tileMapLayer);
            }
        }

        private void RenderTileLayer(TileMapLayer tileMapLayer)
        {
            foreach (var tile in tileMapLayer.Tiles)
            {
                RenderTile(tile);
            }

        }

        private void RenderTile(Tile tile)
        {
            translatedRectangle.X = tile.WorldXPos;
            translatedRectangle.Y = tile.WorldYPos;
            translatedRectangle.Width = tile.TileWidth;
            translatedRectangle.Height = tile.TileHeight;
           
            sourceRectangle.X = tile.WorldTextureXPos;
            sourceRectangle.Y = tile.WorldTextureYPos;
            sourceRectangle.Width = tile.TextureTileWidth;
            sourceRectangle.Height = tile.TextureTileHeight;

            //Draw.SpriteBatch.Draw(Texture.Texture, RenderPosition + offset, clip, Color, Rotation, Origin - clipOffset, Scale, Effects, 0);
            Draw.SpriteBatch.Draw(
               tile.Texture, translatedRectangle, sourceRectangle, 
               Color.White);
        }
    }
}
