﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BnUp.Src.Assets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoBMFont;
using MonoBMFontUWP.Loader;
using Monocle;

namespace BnUp.Src.Components
{   

    class BMFontComponent : Component
    {
        public BmFont Font { get; set; }
        public string Text { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public BMFontComponent() : base(true, true)
        {         
        }

        public override void Render()
        {
            if (Font != null && Text != null)
                Font.Draw(Text, X, Y, Draw.SpriteBatch);            
        }
    }
}
