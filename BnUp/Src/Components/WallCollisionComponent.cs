﻿using BnUp.Src.Tags;
using BnUp.Src.Util;
using Monocle;
using System;

namespace BnUp.Src.Components
{
    class WallCollisionComponent : Component
    {
        private VelocityComponent _velocityComponent;
        private ZCoordComponent _zCoordComponent;        

        private Hitbox testHitBox = new Hitbox(0, 0, 0, 0);

        public WallCollisionComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            
            base.EntityAwake();
        }

        public override void Update()
        {
            if (_velocityComponent != null && _zCoordComponent != null)
            {
                foreach (var wallEntity in Scene[EntityTags.wallTag])
                {
                    if (!wallEntity.Equals(this.Entity))
                    {
                        HorizontalCheck(_velocityComponent, _zCoordComponent, wallEntity, _velocityComponent.Velocity.X);
                        VerticalCheck(_velocityComponent, _zCoordComponent, wallEntity, _velocityComponent.Velocity.Y);
                    }
                }
            }
        }

        private float HorizontalCheck(VelocityComponent velocityComponent, ZCoordComponent zCoordComponent, Entity wallEntity, float targetX)
        {
            WallDirectionComponent wallDirectionComponent = wallEntity.Get<WallDirectionComponent>();

            if (wallDirectionComponent != null) {
                if (!wallDirectionComponent.BlockRight && velocityComponent.Velocity.X > 0)
                {
                    return targetX;
                }

                if (!wallDirectionComponent.BlockLeft && velocityComponent.Velocity.X < 0)
                {
                    return targetX;
                }
            }

            testHitBox.Set(
                                    _zCoordComponent.BaseCollider.Position.X,
                                    _zCoordComponent.BaseCollider.Position.Y,
                                    _zCoordComponent.BaseCollider.Width,
                                    _zCoordComponent.BaseCollider.Height);

            float positionVariation = (targetX * Engine.DeltaTime);
            testHitBox.Position.X += positionVariation;
            bool collision = testHitBox.Collide(wallEntity);
            if (collision)
            {
                TerrainTypeComponent floorComponent = wallEntity.Get<TerrainTypeComponent>();

                collision =
                    TerrainTypeUtil.CheckFloorType(testHitBox, wallEntity, floorComponent.FloorType);
            }

            if (collision)
            {
                if (Math.Abs(positionVariation) <= 1)
                {
                    targetX = 0;
                }
                else
                {
                    targetX = HorizontalCheck(velocityComponent, zCoordComponent, wallEntity, targetX / 2);
                }
            }

            if (Math.Abs(targetX) < Math.Abs(velocityComponent.Velocity.X))
            {
                velocityComponent.SetVelocityX(targetX);
            }

            return targetX;
        }

        private float VerticalCheck(VelocityComponent velocityComponent, ZCoordComponent zCoordComponent, Entity wallEntity, float targetY)
        {
            //temporary code
            //if (velocityComponent.Velocity.Y >= 0)
            //{
            //return targetY;
            //}

            WallDirectionComponent wallDirectionComponent = wallEntity.Get<WallDirectionComponent>();

            if (wallDirectionComponent != null)
            {

                if (!wallDirectionComponent.BlockUp && velocityComponent.Velocity.Y < 0)
                {
                    return targetY;
                }

                if (!wallDirectionComponent.BlockDown && velocityComponent.Velocity.Y > 0)
                {
                    return targetY;
                }
            }

            testHitBox.Set(
                                    _zCoordComponent.BaseCollider.Position.X,
                                    _zCoordComponent.BaseCollider.Position.Y,
                                    _zCoordComponent.BaseCollider.Width,
                                    _zCoordComponent.BaseCollider.Height);            

            float positionVariation = (targetY * Engine.DeltaTime);
            testHitBox.Position.Y += positionVariation;
            bool collision = testHitBox.Collide(wallEntity);

            if (collision)
            {
                TerrainTypeComponent floorComponent = wallEntity.Get<TerrainTypeComponent>();

                collision =
                    TerrainTypeUtil.CheckFloorType(testHitBox, wallEntity, floorComponent.FloorType);
            }

            if (collision)
            {
                //for vertical movement, must have collision in two colliders

                testHitBox.Set(this.Entity.X, this.Entity.Y, this.Entity.Width, this.Entity.Height);

                testHitBox.Position.Y += positionVariation;
                collision = testHitBox.Collide(wallEntity);

                if (collision)
                {
                    TerrainTypeComponent floorComponent = wallEntity.Get<TerrainTypeComponent>();

                    collision =
                        TerrainTypeUtil.CheckFloorType(testHitBox, wallEntity, floorComponent.FloorType);
                }
            }

            if (collision)
            {
                TerrainTypeComponent floorComponent = wallEntity.Get<TerrainTypeComponent>();

                collision =
                    TerrainTypeUtil.CheckFloorType(testHitBox, wallEntity, floorComponent.FloorType);
            }

            if (collision)
            {
                if (Math.Abs(positionVariation) <= 1)
                {
                    targetY = 0;
                }
                else
                {
                    targetY = VerticalCheck(velocityComponent, zCoordComponent, wallEntity, targetY / 2);
                }
                
            }

            if (Math.Abs(targetY) < Math.Abs(velocityComponent.Velocity.Y))
            {
                velocityComponent.SetVelocityY(targetY);
            }

            return targetY;
        }
    }
}
