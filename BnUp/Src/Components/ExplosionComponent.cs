﻿using BnUp.Src.Assets;
using BnUp.Src.Controllers;
using BnUp.Src.Entities;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class ExplosionComponent : Component
    {
        private DirectionComponent _directionComponent;

        private bool _enabled = true;

        public int Damage { get; set; }

        public Hitbox AttackCollider;
        private Hitbox testBaseHitbox;
        private Hitbox testAttackHitbox;

        public ExplosionComponent() : base(true, true)
        {
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _enabled = true;
            testBaseHitbox = new Hitbox(this.Entity.Width, 20);
            testAttackHitbox = new Hitbox(this.Entity.Width, this.Entity.Height);
            base.EntityAwake();
        }

        public override void Update()
        {
            if (_enabled)
            {
                testBaseHitbox.Position.X = this.Entity.Position.X;
                testBaseHitbox.Position.Y = this.Entity.Position.Y + this.Entity.Height - testBaseHitbox.Height * 2;
                testAttackHitbox.Position.X = this.Entity.Position.X;
                testAttackHitbox.Position.Y = this.Entity.Position.Y;

                foreach (var otherEntity in Scene[EntityTags.fighterTag])
                {
                    if (!otherEntity.Equals(this.Entity))
                    {
                        FighterAttributesComponent targetAttributes = otherEntity.Get<FighterAttributesComponent>();

                        if (targetAttributes.State != Enums.FighterState.Faint)
                        {
                            CheckAttack(otherEntity);
                        }
                    }
                }

                foreach (var otherEntity in Scene[EntityTags.breakableTag])
                {
                    if (!otherEntity.Equals(this.Entity))
                    {
                        BreakableAttributesComponent targetAttributes = otherEntity.Get<BreakableAttributesComponent>();

                        if (targetAttributes != null)
                        {
                            CheckAttack(otherEntity);
                        }
                    }
                }

                CreateImpactEffects();
                _enabled = false;
            }
        }

        private void CheckAttack(Entity otherEntity)
        {
            Collider otherCollider = otherEntity.Get<ZCoordComponent>().BaseCollider;
            bool collideBase = testBaseHitbox.Collide(otherCollider);

            bool collideAttack =
                testAttackHitbox.Collide(otherEntity.Collider);

            if (collideBase && collideAttack)
            {
                Direction direction = this.Entity.CenterX < otherEntity.CenterX ? Direction.Right : Direction.Left;

                if (otherEntity.TagCheck(EntityTags.fighterTag))
                {
                    FighterReceiveAttackComponent receiveAttackComponent =
                    otherEntity.Components.Get<FighterReceiveAttackComponent>();

                    if (receiveAttackComponent != null)
                    {
                        receiveAttackComponent.GetHit
                            (Damage,
                            true, direction);
                    }
                }

                if (otherEntity.TagCheck(EntityTags.breakableTag))
                {
                    BreakableReceiveAttackComponent receiveAttackComponent =
                    otherEntity.Components.Get<BreakableReceiveAttackComponent>();

                    if (receiveAttackComponent != null)
                    {
                        receiveAttackComponent.GetHit
                             (Damage,
                             true, direction);
                    }
                }
            }

        }

        private void CreateImpactEffects()
        {
            SoundController.Instance.PlaySound(Enums.SoundKey.PunchImpact);
            //Engine.FreezeTimer = 0.1f;

            foreach (var c in Scene[EntityTags.cameraTag])
            {
                if (c.Get<Shaker>() != null)
                {
                    c.Get<Shaker>().ShakeFor(0.5f, false);
                }
            }

        }

        public override void DebugRender(Camera camera)
        {
            base.DebugRender(camera);
            testAttackHitbox.Render(camera, Color.Green);
            testBaseHitbox.Render(camera, Color.Purple);
        }

    }
}
