﻿using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class PlayerInputComponent : Component
    {
        public int GamePadIndex { get; private set; }
        public VirtualJoystick VirtualJoystick { get; private set; }
        public VirtualButton JumpInput { get; private set; }
        public VirtualButton AttackInput { get; private set; }
        public VirtualButton SpecialInput { get; private set; }
        public bool LockControls { get; set; }

        public PlayerInputComponent(int gamePadIndex = 0) : base(false, false)
        {
            GamePadIndex = gamePadIndex;
            SetupInput(gamePadIndex);
        }

        private void SetupInput(int gamePadIndex)
        {            
            SetupInputJump(gamePadIndex);
            SetupInputMove(gamePadIndex);
            SetupInputAttack(gamePadIndex);
            SetupInputSpecial(gamePadIndex);
        }

        void SetupInputJump(int gamePadIndex)
        {
            JumpInput = new VirtualButton();
            JumpInput.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Space));
            JumpInput.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.A));
        }

        public void SetupInputMove(int gamePadIndex)
        {
            VirtualJoystick = new VirtualJoystick(true);

            VirtualJoystick.Nodes.Add(new VirtualJoystick.KeyboardKeys(
                VirtualInput.OverlapBehaviors.TakeNewer,
                Keys.A,
                Keys.D,
                Keys.W,
                Keys.S));
            VirtualJoystick.Nodes.Add(new VirtualJoystick.PadLeftStick(gamePadIndex, 0.4f));
            VirtualJoystick.Nodes.Add(new VirtualJoystick.PadDpad(gamePadIndex));

        }

        private void SetupInputAttack(int gamePadIndex)
        {
            AttackInput = new VirtualButton();
            AttackInput.Nodes.Add(new VirtualButton.KeyboardKey(Keys.K));
            AttackInput.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.X));
        }

        private void SetupInputSpecial(int gamePadIndex)
        {
            SpecialInput = new VirtualButton();
            SpecialInput.Nodes.Add(new VirtualButton.KeyboardKey(Keys.I));
            SpecialInput.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.Y));
        }

    }
}
