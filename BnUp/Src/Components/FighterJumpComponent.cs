﻿using BnUp.Src.Data;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterJumpComponent : Component
    {
        private ZCoordComponent _zCoordComponent;
        private VelocityComponent _velocityComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;        

        public int JumpVelocity { get; private set; }

        private float currentJumpDelay = 0f;
        private float jumpDelay = 0.15f;

        private Vector2 storedVelocity = new Vector2();

        private bool jumpActive = false;

        public string StartAnimationName { get; private set; }

        private const int MaxVelocityX = 200;

        public FighterJumpComponent(JumpData jumpData) : base(true, false)
        {
            JumpVelocity = jumpData.Height;
            jumpDelay = jumpData.Delay;
            StartAnimationName = jumpData.StartJumpAnimation;
        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();            

            base.EntityAwake();
        }

        public override void Update()
        {
            if (jumpActive)
            {
                if (_fighterAttributesComponent.State != Enums.FighterState.Standing)
                {
                    jumpActive = false;
                }
                else
                {
                    if (currentJumpDelay > 0)
                    {
                        currentJumpDelay -= Engine.DeltaTime;
                        _velocityComponent.SetVelocity(0, 0);

                        if (currentJumpDelay <= 0)
                        {
                            Jump();
                            jumpActive = false;
                        }
                    }
                }
            }
        }

        public void StartJump()
        {
            jumpActive = true;
            currentJumpDelay = jumpDelay;
            storedVelocity.X = _velocityComponent.Velocity.X;
            storedVelocity.Y = _velocityComponent.Velocity.Y;

            if (MathF.Abs(storedVelocity.X) > MaxVelocityX)            
                storedVelocity.X = storedVelocity.X > 0 ? MaxVelocityX : -MaxVelocityX;            

            if (_fighterAttributesComponent.State == Enums.FighterState.Dashing)
            {
                _fighterAttributesComponent.State = Enums.FighterState.Standing;                
            }
        }

        private void Jump()
        {
            _zCoordComponent.ZVelocity = -JumpVelocity;
            _velocityComponent.SetVelocity(storedVelocity.X, storedVelocity.Y);            
        }

        internal bool IsStartingJump()
        {
            return jumpActive;
        }
    }
}
