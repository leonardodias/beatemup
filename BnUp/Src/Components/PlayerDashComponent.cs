﻿using BnUp.Src.Enums;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class PlayerDashComponent : Component
    {
        private ZCoordComponent _zCoordComponent;
        private VelocityComponent _velocityComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private PlayerInputComponent _playerInputComponent;
        private FighterMovementComponent _fighterMovementComponent;
        private Sprite _sprite;
        private FighterDashComponent _fighterDashComponent;

        private const float MaxInputInterval = 0.3f;
        private float inputInterval;
        private int inputCountLeft;
        private int inputCountRight;
        private int previousValue;

        public PlayerDashComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _playerInputComponent = this.Entity.Components.Get<PlayerInputComponent>();
            _fighterMovementComponent = this.Entity.Components.Get<FighterMovementComponent>();
            _fighterDashComponent = this.Entity.Components.Get<FighterDashComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (!_playerInputComponent.LockControls &&
                _groundCollsionComponent.OnGround
                && _fighterAttributesComponent.State == Enums.FighterState.Standing
                && _fighterDashComponent != null)
            {
                int joystickValue = (int)MathF.Round(_playerInputComponent.VirtualJoystick.Value.X);

                if (joystickValue != previousValue)
                {
                    inputInterval = 0;

                    if (joystickValue == -1)
                    {
                        inputCountLeft++;
                        inputCountRight = 0;
                    }
                    if (joystickValue == 1)
                    {
                        inputCountLeft = 0;
                        inputCountRight++;
                    }
                }

                inputInterval += Engine.DeltaTime;

                if (inputInterval > MaxInputInterval)
                {
                    inputCountLeft = 0;
                    inputCountRight = 0;
                }

                if (inputCountLeft >= 2)
                {
                    _fighterDashComponent.StartDash(Direction.Left);
                    inputCountLeft = 0;
                    inputCountRight = 0;
                }

                if (inputCountRight >= 2)
                {
                    _fighterDashComponent.StartDash(Direction.Right);
                    inputCountLeft = 0;
                    inputCountRight = 0;
                }

                previousValue = joystickValue;
            }
        }

    }
}
