﻿using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class SmallJumpClimbDownWallComponent : Component
    {
        private VelocityComponent _velocityComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;

        private Hitbox testHitBox = new Hitbox(0, 0, 0, 0);

        private const float JUMP_DELAY = 0.2f;
        private float _currentJumpDelay = 0.2f;

        private int _jumpVelocity = 200;

        private float disableComponentTime = 0f;

        public SmallJumpClimbDownWallComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (disableComponentTime <= 0)
            {
                if (_velocityComponent != null && _zCoordComponent != null && _groundCollsionComponent != null)
                {
                    if (_velocityComponent.Velocity.Y > 0 && _groundCollsionComponent.OnGround)
                    {
                        bool forcingWall = false;

                        foreach (var wallEntity in Scene[EntityTags.wallTag])
                        {
                            if (!wallEntity.Equals(this.Entity))
                            {
                                testHitBox.Set(
                                    _zCoordComponent.BaseCollider.Position.X,
                                    _zCoordComponent.BaseCollider.Position.Y,
                                    _zCoordComponent.BaseCollider.Width,
                                    _zCoordComponent.BaseCollider.Height);

                                float positionVariation = ((float)Math.Round(_velocityComponent.Velocity.Y * Engine.DeltaTime));
                                testHitBox.Position.Y += positionVariation;
                                bool collision = testHitBox.Collide(wallEntity);

                                if (collision)
                                {
                                    forcingWall = true;
                                }
                            }
                        }

                        if (forcingWall)
                        {
                            _currentJumpDelay -= Engine.DeltaTime;

                            if (_currentJumpDelay <= 0)
                            {
                                _zCoordComponent.ZVelocity = -_jumpVelocity;
                                _currentJumpDelay = JUMP_DELAY;
                                disableComponentTime = 1f;
                            }
                        }
                        else
                        {
                            _currentJumpDelay = JUMP_DELAY;
                        }

                    }

                }
            }
            else
            {
                disableComponentTime -= Engine.DeltaTime;
            }
        }
    }
}
