﻿using BnUp.Src.Assets;
using BnUp.Src.Data;
using BnUp.Src.Entities;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterGrabThrowComponent : Component
    {
        private const int GrabPointDistance = 6;
        private Vector2 grabPoint = new Vector2();
        private Vector2 testCenter = new Vector2();

        private DirectionComponent _directionComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private FighterAttributesComponent _fighterAttributesComponent;

        public bool GrabLifting { get; set; }

        private Entity target;
        private Direction throwDirection;
        private int throwState = 0;
        private float currentThrowDelay;
        private float throwDelay = 0.2f;
        private float currentThrowCooldown;
        private float throwCooldown = 0.2f;
        private int targetX;
        private int targetY;
        private int targetZ;

        private ThrowData backThrow;
        private ThrowData frontThrow;

        public FighterGrabThrowComponent(FighterData fighterData) : base(true, false)
        {
            GrabLifting = fighterData.GrabLifting;
            backThrow = fighterData.BackThrow;
            frontThrow = fighterData.FrontThrow;
        }

      //  public FighterGrabThrowComponent(bool grabLifting = false) : base(true, false)
      //  {
      //      GrabLifting = grabLifting;
      //  }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();

            base.EntityAwake();
        }

        internal void StartThrow(Direction direction)
        {
            if (target != null)
            {
                _fighterAttributesComponent.State = Enums.FighterState.Throwing;

                ThrowData throwData = (direction == _directionComponent.Direction ? frontThrow : backThrow);

                currentThrowDelay = throwData.Delay;
                currentThrowCooldown = throwData.Cooldown;
                throwDirection = direction;
                targetX = throwData.EnemyPosition.X + (int) this.Entity.X;
                targetY = throwData.EnemyPosition.Y + (int) this.Entity.Y;
                targetZ = throwData.EnemyPosition.Z + _zCoordComponent.ZValue;

                throwState = 0;
            }
        }

        public override void Update()
        {
            UpdateGrabPointPosition();

            if (_groundCollsionComponent.OnGround
                && _fighterAttributesComponent.State == Enums.FighterState.Standing)
            {
                Entity fighterTarget = CheckGrabCollisions();

                if (fighterTarget != null
                    && fighterTarget.Get<FighterAttributesComponent>().State == Enums.FighterState.Standing
                    && fighterTarget.Get<GroundCollsionComponent>().OnGround)
                {
                    if (MovingTowardsTarget(fighterTarget))
                    {
                        Grab(fighterTarget);
                        this.target = fighterTarget;
                    }
                }
            }

            if (_fighterAttributesComponent.State == Enums.FighterState.Grabbing)
            {
                if (target == null)
                {
                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                }
                else
                {
                    if (target.Get<FighterAttributesComponent>().State != Enums.FighterState.Grabbed
                        && target.Get<FighterAttributesComponent>().State != Enums.FighterState.GrabbedLifted)
                    {
                        _fighterAttributesComponent.State = Enums.FighterState.Standing;
                        target = null;
                    }
                }
            }

            if (_fighterAttributesComponent.State == Enums.FighterState.Throwing)
            {
                switch (throwState)
                {
                    case 0:
                        currentThrowDelay -= Engine.DeltaTime;

                        target.Get<ZCoordComponent>().BaseCollider.Position.X = targetX;
                        target.Get<ZCoordComponent>().BaseCollider.Position.Y = targetY;
                        target.Get<ZCoordComponent>().ZValue = targetZ;

                        if (currentThrowDelay <= 0)
                        {
                            target.Get<FighterReceiveAttackComponent>().GetHit(10, true, throwDirection, 400);
                            CreateAttack();
                            throwState = 1;
                        }

                        break;
                    case 1:
                        currentThrowCooldown -= Engine.DeltaTime;

                        if (currentThrowCooldown <= 0)
                        {
                            _fighterAttributesComponent.State = Enums.FighterState.Standing;
                            throwState = 2;
                        }


                        break;
                    case 2:

                        break;
                }

               
            }

        }

        private Entity CheckGrabCollisions()
        {
            foreach (var fighterEntity in Scene[EntityTags.fighterTag])
            {
                if (!fighterEntity.Equals(this.Entity) && !fighterEntity.CollideCheck(this.Entity))
                {
                    GrabbableComponent grabbable = fighterEntity.Get<GrabbableComponent>();

                    if (grabbable != null)
                    {
                        if (grabbable.FrontGrab.Collide(grabPoint)
                            || grabbable.RearGrab.Collide(grabPoint))
                        {
                            return fighterEntity;
                        }
                    }
                }
            }

            return null;
        }

        public Entity GetTarget()
        {
            return target;
        }

        public void Grab(Entity figthterTarget)
        {
            if (figthterTarget != null)
            {
                if (figthterTarget.Get<FighterAttributesComponent>().State == Enums.FighterState.Standing
                    || figthterTarget.Get<FighterAttributesComponent>().State == Enums.FighterState.Hurt)
                {
                    GrabbableComponent grabbableComponent = figthterTarget.Get<GrabbableComponent>();
                    bool rearGrab = grabbableComponent.RearGrab.Collide(grabPoint);

                    figthterTarget.Get<VelocityComponent>().SetVelocity(0, 0);
                    _velocityComponent.SetVelocity(0, 0);
                    figthterTarget.Get<ZCoordComponent>().ZVelocity = 0;

                    if (rearGrab)
                        figthterTarget.Get<DirectionComponent>().TurnDirection();

                    if (GrabLifting)
                    {
                        figthterTarget.Get<FighterAttributesComponent>().State = Enums.FighterState.GrabbedLifted;
                    }
                    else
                    {
                        figthterTarget.Get<FighterAttributesComponent>().State = Enums.FighterState.Grabbed;
                    }
                    _fighterAttributesComponent.State = Enums.FighterState.Grabbing;

                    grabbableComponent.GrabbingEntity = this.Entity;
                }
               
            }
        }

        private bool MovingTowardsTarget(Entity fighterTarget)
        {            
            float actualDistance = Vector2.Distance(this.Entity.Center, fighterTarget.Center);

            testCenter.X = this.Entity.Center.X + _velocityComponent.Velocity.X * Engine.DeltaTime;
            testCenter.Y = this.Entity.Center.Y + _velocityComponent.Velocity.Y * Engine.DeltaTime;

            float futureDistance = Vector2.Distance(testCenter, fighterTarget.Center);

            return futureDistance < actualDistance;            
        }

        private void UpdateGrabPointPosition()
        {
            if (_directionComponent.Direction == Enums.Direction.Left)
                grabPoint.X = this.Entity.Position.X - GrabPointDistance;
            else
                grabPoint.X = this.Entity.Position.X + this.Entity.Width + GrabPointDistance;

            grabPoint.Y = this.Entity.Position.Y + this.Entity.Height / 2;
        }

        private void CreateAttack()
        {
            ThrowAttackEntity attack = Engine.Pooler.Create<ThrowAttackEntity>();

            int w = 24;
            int h = 8;

            attack.Collider = new Hitbox(w, h);
            attack.Position.X = MathF.Round(target.Collider.Position.X);
            attack.Position.Y = MathF.Round(target.Collider.Position.Y);

            ThrowAttackComponent throwAttackComponent = attack.Get<ThrowAttackComponent>();

            throwAttackComponent.Owner = this.Entity;
            throwAttackComponent.ThrowedFighter = target;
            throwAttackComponent.Damage = 5;

            TTLComponent tLComponent = new TTLComponent();
            tLComponent.SetTTL(1f);
            attack.Add(tLComponent);

            attack.Get<DirectionComponent>().Direction = throwDirection;

            EntityPinComponent entityPin = new EntityPinComponent();
            entityPin.Target = target;
            entityPin.SetRelativePosition(Direction.Left, 0, 0);
            entityPin.SetRelativePosition(Direction.Right, 0, 0);
            entityPin.FollowZBase = true;
            attack.Add(entityPin);

            Scene.Add(attack);
        }
    }
}