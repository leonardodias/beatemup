﻿using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class ParallaxComponent : Component
    {
        private CameraMovementComponent cameraMovementComponent;

        private float _startPosX;
        private float _startCameraX;
        private int _distance;

        public ParallaxComponent(int distance) : base(true, false)
        {
            _distance = distance;
        }

        public override void EntityAwake()
        {
            _startPosX = this.Entity.Position.X;
            base.EntityAwake();
        }

        public override void Update()
        {
            if (cameraMovementComponent == null)
            {
                cameraMovementComponent = Scene[EntityTags.cameraTag][0].Get<CameraMovementComponent>();
                _startCameraX = cameraMovementComponent.GetCameraBounds().X;
            }
            else
            {
                float cameraX = cameraMovementComponent.GetCameraBounds().X;
                float newPosition = _startPosX + (cameraMovementComponent.GetCameraBounds().X - _startCameraX) * _distance / 10;
                this.Entity.Position.X = newPosition;
            }
        }
    }
}