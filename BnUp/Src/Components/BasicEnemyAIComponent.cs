﻿using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class BasicEnemyAIComponent : Component
    {
        private VelocityComponent _velocityComponent;
        private ZCoordComponent _zCoordComponent;
        private FighterAttributesComponent _fighterStateComponent;
        private DirectionComponent _directionComponent;
        private FighterMovementComponent _fighterMovementComponent;

        Entity playerTarget;

        private Vector2 targetPoint = new Vector2();
        private Vector2 targetVelocity = new Vector2();

        private FighterAttackComponent _fighterAttackComponent;

        private Hitbox playerSurroundings = new Hitbox(50, 20);
        private Hitbox aleatoryDestinationCheck = new Hitbox(16, 16);

        private Random random;

        private int state = 1;

        private float stateTime;

        private const int targetDistance = 14;

        private Vector2 aleatoryDirection = new Vector2();

        private float refreshTargetTime = 0;
        private float refreshDirection = 0;
        private bool readyToAttack = false;

        public BasicEnemyAIComponent() : base(true, false)
        {
            this.random = new Random();
        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _fighterStateComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _fighterMovementComponent = this.Entity.Components.Get<FighterMovementComponent>();
            _fighterAttackComponent = this.Entity.Components.Get<FighterAttackComponent>();


            base.EntityAwake();
        }

        public override void Update()
        {
            if (_fighterStateComponent.State == Enums.FighterState.KnockedOut)
            {
                state = 0;
                stateTime = 0;
            }

            switch (state)
            {
                case 0:
                    _fighterMovementComponent.SetMovement(0, 0);

                    stateTime += Engine.DeltaTime;
                    if (stateTime > random.NextFloat(2) + 2)
                    {
                        ChangeAIState();
                    }

                    break;

                case 1:


                    stateTime += Engine.DeltaTime;
                    if (stateTime > 3)
                    {
                        ChangeAIState();
                    }

                    refreshTargetTime += Engine.DeltaTime;

                    if (refreshTargetTime > 0.5f)
                    {
                        refreshTargetTime = 0;
                        FindNearestPlayer();
                    }

                    if (_fighterStateComponent.State == Enums.FighterState.Standing)
                    {
                        if (playerTarget != null)
                        {
                            if (playerTarget.X > this.Entity.X)
                            {
                                _directionComponent.Direction = Enums.Direction.Right;

                            }
                            else
                            {
                                _directionComponent.Direction = Enums.Direction.Left;
                            }
                        }

                        //                        if (playerTarget != null &&
                        //(Vector2.Distance(playerTarget.CenterRight, this.Entity.CenterLeft) < 12
                        //|| (Vector2.Distance(playerTarget.CenterLeft, this.Entity.CenterRight) < 12)))  
                        if (playerTarget != null &&
                        (MathF.Abs(playerTarget.CenterRight.X - this.Entity.CenterLeft.X) < targetDistance + 2
                        || (MathF.Abs(playerTarget.CenterLeft.X - this.Entity.CenterRight.X) < targetDistance + 2))
                        && MathF.Abs(playerTarget.Center.Y - this.Entity.CenterLeft.Y) < 2)
                        {
                            _fighterMovementComponent.SetMovement(0, 0);
                            readyToAttack = true;
                            if (playerTarget.CenterX < this.Entity.CenterX)
                                _directionComponent.Direction = Enums.Direction.Left;
                            else
                                _directionComponent.Direction = Enums.Direction.Right;

                        }
                        else
                        {
                            refreshDirection -= Engine.DeltaTime;

                            if (refreshDirection <= 0)
                            {
                                targetVelocity.X = targetPoint.X - this.Entity.X;
                                targetVelocity.Y = targetPoint.Y - this.Entity.Y;

                                if (!(targetVelocity.X == 0 && targetVelocity.Y == 0))
                                    targetVelocity.Normalize();

                                refreshDirection = 0.2f;
                            }                            

                            _fighterMovementComponent.SetMovement(targetVelocity.X, targetVelocity.Y);
                        }

                        if (readyToAttack)
                        {
                            _fighterAttackComponent.StartAttack();
                            ChangeAIState();
                        }
                    }

                    break;

                case 2:
                    stateTime += Engine.DeltaTime;
                    if (stateTime > 1)
                    {
                        ChangeAIState();
                    }

                    _fighterMovementComponent.SetMovement(aleatoryDirection.X, aleatoryDirection.Y);


                    break;

                default:

                    break;
            }


        }

        private void ChangeAIState()
        {
            stateTime = 0;

            int lastState = state;
            state = random.Next(0, 2);

            if (state == lastState)
                state++;

            if (state > 2)
                state = 0;

            switch (state)
            {
                case 0:


                    break;
                case 1:
                    readyToAttack = false;
                    FindNearestPlayer();

                    if (playerTarget == null)
                        state = 0;

                    break;

                case 2:

                    ChooseAleatoryDirection();
/*                    aleatoryDestinationCheck.Position.X = this.Entity.CenterX + aleatoryDirection.X * 32;
                    aleatoryDestinationCheck.Position.Y = this.Entity.CenterY + aleatoryDirection.Y * 32;
                    
                    bool movingTowardsPlayer = false;
                    foreach (var playerEntity in Scene[EntityTags.playerTag])
                    {
                        if (aleatoryDestinationCheck.Collide(playerEntity))
                            movingTowardsPlayer = true;
                    }

                    if (movingTowardsPlayer)
                        aleatoryDirection.Rotate(3.14159f);*/

                    break;

                default:

                    break;
            }
        }

        private void ChooseAleatoryDirection()
        {
            aleatoryDirection.X = random.NextFloat(2) - 1;
            aleatoryDirection.Y = random.NextFloat(2) - 1;

            aleatoryDirection.Normalize();
        }

        private Entity FindNearestPlayer()
        {
            float minorDistance = 9999;

            playerTarget = null;

            foreach (var playerEntity in Scene[EntityTags.playerTag])
            {
                if (!playerEntity.Active)
                    continue;

                if (playerEntity.Get<FighterAttributesComponent>().State == Enums.FighterState.KnockedOut
                    || playerEntity.Get<FighterAttributesComponent>().State == Enums.FighterState.Faint)
                    continue;

                bool leftSurrounded = false;
                bool rightSurrounded = false;

                playerSurroundings.Position.X = playerEntity.Get<ZCoordComponent>().BaseCollider.Center.X - playerSurroundings.Width;
                playerSurroundings.Position.Y = playerEntity.Get<ZCoordComponent>().BaseCollider.Center.Y - playerSurroundings.Height / 2;

                foreach (var otherEnemy in Scene[EntityTags.enemyTag])
                {
                    if (!this.Entity.Equals(otherEnemy) && playerSurroundings.Collide(otherEnemy))
                        leftSurrounded = true;
                }

                playerSurroundings.Position.X = playerEntity.Get<ZCoordComponent>().BaseCollider.Center.X;
                playerSurroundings.Position.Y = playerEntity.Get<ZCoordComponent>().BaseCollider.Center.Y - playerSurroundings.Height / 2;

                foreach (var otherEnemy in Scene[EntityTags.enemyTag])
                {
                    if (!this.Entity.Equals(otherEnemy) && playerSurroundings.Collide(otherEnemy))
                        rightSurrounded = true;
                }

                if (!leftSurrounded)
                {
                    float distance = Vector2.Distance(this.Entity.CenterLeft, playerEntity.Center);

                    if (distance < minorDistance)
                    {
                        playerTarget = playerEntity;
                        targetPoint.X = playerEntity.X - this.Entity.Width - targetDistance;
                        targetPoint.Y = playerEntity.Y;
                    }
                }

                if (!rightSurrounded)
                {
                    float distance = Vector2.Distance(this.Entity.CenterRight, playerEntity.Center);

                    if (distance < minorDistance)
                    {
                        playerTarget = playerEntity;
                        targetPoint.X = playerEntity.X + playerEntity.Width + targetDistance;
                        targetPoint.Y = playerEntity.Y;
                    }
                }
            }

            return playerTarget;
        }
    }
}
