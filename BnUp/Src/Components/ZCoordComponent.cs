﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class ZCoordComponent : Component
    {
        public int ZValue { get; set; }
        public float ZVelocity { get; set; }

        public Collider BaseCollider { get; }        

        public ZCoordComponent(int w = 16, int h = 12) : base(true, true)
        {
            BaseCollider = new Hitbox(w, h);
        }

        public override void Update()
        {
            ZValue += ((int)Math.Round(ZVelocity * Engine.DeltaTime));

            this.Entity.Position.X = BaseCollider.Position.X;
            this.Entity.Position.Y = BaseCollider.Position.Y + ZValue - this.Entity.Height + BaseCollider.Height;

            if (ZValue > 300)
            {
                this.Entity.RemoveSelf();            
            }
        }
        
        public override void DebugRender(Camera camera)
        {
            base.DebugRender(camera);
            BaseCollider.Render(camera, Color.DarkOrange);
        }

        public override void Render()
        {
            base.Render();
        }
    }
}
