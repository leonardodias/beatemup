﻿using BnUp.Src.Data;
using BnUp.Src.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterAttackComponent : Component
    {
        private List<AttackData> _standingAttackDataList = new List<AttackData>();
        private List<AttackData> _airAttackDataList = new List<AttackData>();
        private List<AttackData> _specialAttackDataList = new List<AttackData>();
        private List<AttackData> _grabAttackDataList = new List<AttackData>();

        private AttackData _currentAttack;

        private bool _addCombo = false;
        private int _standingComboIndex = 0;
        private int _grabComboIndex = 0;
        private int _airComboIndex = 0;
        private float _comboTime = 0.7f;
        private float _currentComboTime;

        private DirectionComponent _directionComponent;

        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private FighterJumpComponent _fighterJumpComponent;
        private Sprite _sprite;

        private int attackState = 0;

        private float currentAttackDelayTime = 0f;
        private float currentAttackDurationTime = 0f;
        private float currentAttackCooldownTime = 0f;

        private bool isAttacking = false;

        //public Collider AttackCollider { get; }
        public bool AttackActive { get; private set; }

        private bool grabAttack = false;
        private bool dashAttack = false;
        private bool airAttack = false;

        public FighterAttackComponent(FighterData data) : base(true, true)
        {
            _standingAttackDataList.AddRange(data.StandingAttacks);
            _airAttackDataList.AddRange(data.AirAttacks);
            _grabAttackDataList.AddRange(data.GrabAttacks);
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();
            _fighterJumpComponent = this.Entity.Components.Get<FighterJumpComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            UpdateComboCount();

            if (isAttacking)
            {
                switch (attackState)
                {
                    case 0:

                        if (!_fighterJumpComponent.IsStartingJump())
                        {
                            attackState = 1;

                            if (grabAttack)
                                _currentAttack = _grabAttackDataList[_grabComboIndex];
                            else if (_groundCollsionComponent.OnGround && _zCoordComponent.ZVelocity >= 0)
                                _currentAttack = _standingAttackDataList[_standingComboIndex];
                            else
                            {
                                if (_airComboIndex < _airAttackDataList.Count)
                                    _currentAttack = _airAttackDataList[_airComboIndex];
                            }

                            if (_currentAttack != null)
                            {
                                currentAttackDelayTime = _currentAttack.Delay;

                                _fighterAttributesComponent.State = Enums.FighterState.Attacking;
                                _sprite.Play(_currentAttack.AnimationName);

                                airAttack = !_groundCollsionComponent.OnGround || _zCoordComponent.ZVelocity < 0;
                            }
                            else
                            {
                                airAttack = false;
                                isAttacking = false;
                                _fighterAttributesComponent.State = Enums.FighterState.Standing;
                                attackState = 0;
                            }
                        }

                        break;
                    case 1:
                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {
                            if (_groundCollsionComponent.OnGround && _zCoordComponent.ZVelocity >= 0
                                && !dashAttack)
                                _velocityComponent.SetVelocity(0, 0);
                            else
                                _velocityComponent.SetVelocityY(0);

                            currentAttackDelayTime -= Engine.DeltaTime;

                            if (currentAttackDelayTime <= 0)
                            {
                                if (airAttack || _groundCollsionComponent.OnGround)
                                {
                                    attackState = 2;
                                    currentAttackDurationTime = _currentAttack.Duration;
                                    AttackActive = true;
                                    CreateAttack();
                                }
                                else
                                {
                                    airAttack = false;
                                    isAttacking = false;
                                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                                    attackState = 0;
                                }

                            }
                        }
                        else
                            isAttacking = false;

                        break;
                    case 2:

                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {
                            if (_groundCollsionComponent.OnGround)
                            {
                                _velocityComponent.SetVelocity(0, 0);

                                if (airAttack && !_currentAttack.ContinuousAirAttack)
                                {
                                    airAttack = false;
                                    isAttacking = false;
                                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                                    attackState = 0;
                                }
                            }

                            currentAttackDurationTime -= Engine.DeltaTime;

                            if (currentAttackDurationTime <= 0)
                            {
                                attackState = 3;

                                currentAttackCooldownTime = _currentAttack.Cooldown;

                                AttackActive = false;
                            }
                        }
                        else
                            isAttacking = false;

                        break;
                    case 3:


                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {
                            if (_groundCollsionComponent.OnGround)
                            {
                                _velocityComponent.SetVelocity(0, 0);

                                if (airAttack && !_currentAttack.ContinuousAirAttack)
                                {
                                    airAttack = false;
                                    isAttacking = false;
                                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                                    attackState = 0;
                                }
                            }

                            currentAttackCooldownTime -= Engine.DeltaTime;

                            if (currentAttackCooldownTime <= 0)
                            {
                                isAttacking = false;
                                attackState = 0;

                                if (grabAttack && !_currentAttack.KnockDown)
                                {
                                    _fighterAttributesComponent.State = Enums.FighterState.Grabbing;
                                    this.Entity.Get<FighterGrabThrowComponent>().Grab(this.Entity.Get<FighterGrabThrowComponent>().GetTarget());
                                }
                                else
                                    _fighterAttributesComponent.State = Enums.FighterState.Standing;
                            }
                        }
                        else
                            isAttacking = false;

                        break;
                }
            }
            else
            {
                attackState = 0;
            }

        }

        private void UpdateComboCount()
        {
            if (_addCombo)
            {
                _addCombo = false;
                _currentComboTime = _comboTime;

                if (_groundCollsionComponent.OnGround)
                {
                    _standingComboIndex++;
                    _grabComboIndex++;

                    if (_standingComboIndex >= _standingAttackDataList.Count)
                        _standingComboIndex = 0;

                    if (_grabComboIndex >= _grabAttackDataList.Count)
                        _grabComboIndex = 0;
                }
                else
                {
                    _airComboIndex++;

                    if (_airComboIndex >= _airAttackDataList.Count)
                    {
                        _airComboIndex = 0;
                    }
                }
            }

            if (_currentComboTime <= 0)
            {
                _standingComboIndex = 0;
            }
            else
            {
                _currentComboTime -= Engine.DeltaTime;
            }
        }

        internal void StartAttack()
        {
            if (_fighterAttributesComponent.State == Enums.FighterState.Standing
                   || _fighterAttributesComponent.State == Enums.FighterState.Grabbing
                   || _fighterAttributesComponent.State == Enums.FighterState.Dashing)
            {
                isAttacking = true;
                grabAttack = _fighterAttributesComponent.State == Enums.FighterState.Grabbing;
                dashAttack = _fighterAttributesComponent.State == Enums.FighterState.Dashing;
                //_fighterAttributesComponent.State = Enums.FighterState.Attacking;
            }
        }

        private void CreateAttack()
        {
            AttackEntity attack = Engine.Pooler.Create<AttackEntity>();

            attack.Collider = new Hitbox(_currentAttack.AttackArea.X + _currentAttack.AttackArea.Width, _zCoordComponent.BaseCollider.Height);

            AttackComponent attackComponent = attack.Get<AttackComponent>();

            attackComponent.Owner = this.Entity;
            attackComponent.KnockHit = _currentAttack.KnockDown;
            attackComponent.Damage = _currentAttack.Damage;

            attackComponent.AttackCollider = new Hitbox(
                _currentAttack.AttackArea.Width,
                _currentAttack.AttackArea.Height,
                _currentAttack.AttackArea.X,
                _currentAttack.AttackArea.Y
                );

            attack.Get<TTLComponent>().SetTTL(_currentAttack.Duration);

            attack.Get<DirectionComponent>().Direction = _directionComponent.Direction;

            EntityPinComponent entityPin = attack.Get<EntityPinComponent>();
            entityPin.Target = this.Entity;
            entityPin.SetRelativePosition(Enums.Direction.Left, ((int)-attack.Collider.Width), 0);
            entityPin.SetRelativePosition(Enums.Direction.Right, ((int)_zCoordComponent.BaseCollider.Width), 0);
            entityPin.FollowZBase = true;
            attack.Add(entityPin);

            Scene.Add(attack);
        }

        public override void DebugRender(Camera camera)
        {
            base.DebugRender(camera);
        }

        public override void Render()
        {
            base.Render();
        }

        public void AddComboCount()
        {
            this._addCombo = true;
        }
    }
}
