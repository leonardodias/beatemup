﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class TTLComponent : Component
    {
        float _ttlSeconds;
        float _startTTLSeconds;

        public TTLComponent() : base(true, false)
        {            
            _ttlSeconds = _startTTLSeconds;
        }

        public override void EntityAwake()
        {
            _ttlSeconds = _startTTLSeconds;
        }

        public void SetTTL(float value)
        {
            _ttlSeconds = value;
            _startTTLSeconds = value;
        }

        public override void Update()
        {
            _ttlSeconds -= Engine.DeltaTime;

            if (_ttlSeconds < 0)
            {                
                this.Entity.RemoveSelf();
            }
        }
    }
}
