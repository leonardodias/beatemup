﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class GravityComponent : Component
    {
        private float _gravity = 1000;
        private ZCoordComponent _zCoordComponent;

        public GravityComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            base.EntityAwake();
        }

        public override void Update()
        {
            float gravityForce = _gravity * Engine.DeltaTime;

            _zCoordComponent.ZVelocity += gravityForce;
        }


    }
}
