﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;

namespace BnUp.Src.Components
{
    class CameraMovementComponent : Component
    {
        private Camera _camera;        

        public Camera Camera
        {
            get { return _camera; }            
        }

        public int OffsetY = -72;        

        private List<Entity> targetEntities = new List<Entity>();
        private float targetEntityRefreshTime = 0.5f;
        private float currentTargetEntityRefreshTime = 0.5f;
        private Vector2 targetPosition = new Vector2();        

        public Rectangle Limits { get; set; }

        public float CameraVelocity { get; set; }
        private Vector2 cameraDirection = new Vector2();

        private Vector2 _cameraCenterPosition = new Vector2();
        private Rectangle _cameraBounds;

        public CameraMovementComponent(Camera camera) : base(true, false)
        {
            _camera = camera;
            CameraVelocity = 200;
            _cameraBounds = new Rectangle(0, 0, camera.Viewport.Width, camera.Viewport.Height);
        }

        public override void EntityAwake()
        {
            _camera.X = 0;
            _camera.Y = 0;

            base.EntityAwake();
        }

        public override void Update()
        {
            UpdateTargetEntities();

            UpdateTargetPosition();

            MoveCamera();

            if (Limits != null && Limits.Width > 0)
            {
                RestrictToLimits();
            }
            
            //_camera.Y = MathF.Min(_camera.Y, StageSize - _camera.Viewport.Height);
            //_camera.Y = MathF.Max(_camera.Y, 0);
        }

        private void MoveCamera()
        {
            cameraDirection.X = targetPosition.X - _camera.X;
            cameraDirection.Y = targetPosition.Y - _camera.Y;

            cameraDirection.Normalize();

            if (_camera.X != targetPosition.X)
            {
                bool targetGreater = targetPosition.X > _camera.X;
                _camera.X += cameraDirection.X * CameraVelocity * Engine.DeltaTime;
                
                bool targetStillGreater = targetPosition.X > _camera.X;

                if (targetGreater != targetStillGreater)
                    _camera.X = targetPosition.X;
            }

            if (_camera.Y != targetPosition.Y)
            {
                bool targetGreater = targetPosition.Y > _camera.Y;
                _camera.Y += cameraDirection.Y * CameraVelocity * Engine.DeltaTime;

                bool targetStillGreater = targetPosition.Y > _camera.Y;

                if (targetGreater != targetStillGreater)
                    _camera.Y = targetPosition.Y;
            }
        }

        public Vector2 GetCameraCenter()
        {
            _cameraCenterPosition.X = _camera.Position.X + _camera.Viewport.Width / 2;
            _cameraCenterPosition.Y = _camera.Position.Y + _camera.Viewport.Height / 2;

            return _cameraCenterPosition;
        }

        public Rectangle GetCameraBounds()
        {
            _cameraBounds.X = (int) _camera.Position.X;
            _cameraBounds.Y = (int) _camera.Position.Y;

            return _cameraBounds;
        }

        private void RestrictToLimits()
        {
            _camera.X = MathF.Min(_camera.X, Limits.X + Limits.Width - _camera.Viewport.Width);
            _camera.X = MathF.Max(_camera.X, Limits.X);

            _camera.Y = MathF.Min(_camera.Y, Limits.Y + Limits.Height - _camera.Viewport.Height);
            _camera.Y = MathF.Max(_camera.Y, Limits.Y);
        }

        private void UpdateTargetEntities()
        {
            if (currentTargetEntityRefreshTime > 0)
            {
                currentTargetEntityRefreshTime -= Engine.DeltaTime;
            }
            else
            {
                targetEntities.Clear();

                foreach (var item in Scene[EntityTags.playerTag])
                    targetEntities.Add(item);

                currentTargetEntityRefreshTime = targetEntityRefreshTime;
            }
        }

        private void UpdateTargetPosition()
        {
            if (targetEntities.Count() > 0)
            {
                targetPosition.X = 0;
                targetPosition.Y = 0;

                foreach (var player in targetEntities)
                {
                    targetPosition.X += player.Get<ZCoordComponent>().BaseCollider.Center.X;
                    targetPosition.Y += player.Get<ZCoordComponent>().BaseCollider.Center.Y;
                }

                targetPosition.X = MathF.Round(targetPosition.X / targetEntities.Count());
                targetPosition.Y = MathF.Round(targetPosition.Y / targetEntities.Count());

                targetPosition.X -= _camera.Viewport.Width / 2;
                targetPosition.Y -= _camera.Viewport.Height / 2;
                targetPosition.Y += OffsetY;
            }

        }
    }
}
