﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterFaintingComponent : Component
    {
        private FighterAttributesComponent _fighterAttributesComponent;

        private float blinkingTotalTime = 1f;

        private float currentBlinkTime = 0f;
        private float blinkTime = 0.1f;


        public FighterFaintingComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_fighterAttributesComponent.State == Enums.FighterState.Faint)
            {
                blinkingTotalTime -= Engine.DeltaTime;

                if (blinkingTotalTime <= 0)
                {
                    this.Entity.Active = false;
                    this.Entity.RemoveSelf();
                }
                else
                {
                    currentBlinkTime -= Engine.DeltaTime;

                    if (currentBlinkTime <= 0)
                    {
                        this.Entity.Visible = !this.Entity.Visible;
                        currentBlinkTime += blinkTime;
                    }
                }
            }


        }
    }
}
