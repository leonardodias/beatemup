﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class VelocityComponent : Component
    {
        private const int fraction = 10;

        private Vector2 _velocity = new Vector2();

        private ZCoordComponent _zCoordComponent;

        public Vector2 Velocity
        {
            get { return _velocity; }
        }

        public VelocityComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            base.EntityAwake();
        }

        public void SetVelocity(float x, float y)
        {
            _velocity.X = x;
            _velocity.Y = y;
        }

        public void AddVelocity(float x, float y)
        {
            _velocity.X += x;
            _velocity.Y += y;
        }

        public void SetVelocityX(float v)
        {
            _velocity.X = v;
        }

        public void SetVelocityY(float v)
        {            
            _velocity.Y = v;
        }

        public override void Update()
        {
            _zCoordComponent.BaseCollider.Position.X += (_velocity.X * Engine.DeltaTime);
            _zCoordComponent.BaseCollider.Position.Y += (_velocity.Y * Engine.DeltaTime);

            _zCoordComponent.BaseCollider.Position.X *= fraction;
            _zCoordComponent.BaseCollider.Position.Y *= fraction;

            _zCoordComponent.BaseCollider.Position.X = MathF.Round(_zCoordComponent.BaseCollider.Position.X);
            _zCoordComponent.BaseCollider.Position.Y = MathF.Round(_zCoordComponent.BaseCollider.Position.Y);

            _zCoordComponent.BaseCollider.Position.X /= fraction;
            _zCoordComponent.BaseCollider.Position.Y /= fraction;

            //this.Entity.Position.X += (_velocity.X * Engine.DeltaTime);
            //this.Entity.Position.Y += (_velocity.Y * Engine.DeltaTime);
        }


    }
}
