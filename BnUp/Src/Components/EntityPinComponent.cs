﻿using BnUp.Src.Enums;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class EntityPinComponent : Component
    {

        private Entity target;
        private ZCoordComponent targetZCoord;

        public Entity Target
        {
            get { return target; }
            set
            {
                target = value;

                if (target != null)
                    targetZCoord = target.Get<ZCoordComponent>();
            }
        }

        private Vector2 _rightRelativePosition = new Vector2();
        private Vector2 _leftRelativePosition = new Vector2();

        public bool FollowZBase { get; set; }

        public Vector2 RelativePosition
        {
            get
            {
                if (Target != null
                    && Target.Get<DirectionComponent>() != null
                    && Target.Get<DirectionComponent>().Direction == Direction.Left)
                {
                    return _leftRelativePosition;
                }
                else
                {
                    return _rightRelativePosition;
                }
            }
        }


        public EntityPinComponent() : base(true, false)
        {

        }


        public override void Update()
        {
            if (Target != null)
            {
                if (!Target.Active)
                {
                    this.Entity.Active = false;
                    this.Entity.RemoveSelf();
                }

                if (Target.Get<DirectionComponent>().Direction == Direction.Left)
                {
                    if (FollowZBase)
                    {
                        this.Entity.Position.X = targetZCoord.BaseCollider.Position.X + _leftRelativePosition.X;
                        this.Entity.Position.Y = targetZCoord.BaseCollider.Position.Y + _leftRelativePosition.Y;
                    }
                    else
                    {
                        this.Entity.Position.X = Target.Position.X + _leftRelativePosition.X;
                        this.Entity.Position.Y = Target.Position.Y + _leftRelativePosition.Y;
                    }
                }
                else
                {
                    if (FollowZBase)
                    {
                        this.Entity.Position.X = targetZCoord.BaseCollider.Position.X + _rightRelativePosition.X;
                        this.Entity.Position.Y = targetZCoord.BaseCollider.Position.Y + _rightRelativePosition.Y;
                    }
                    else
                    {
                        this.Entity.Position.X = Target.Position.X + _rightRelativePosition.X;
                        this.Entity.Position.Y = Target.Position.Y + _rightRelativePosition.Y;
                    }
                }
            }
        }

        public void SetRelativePosition(Direction direction, int x, int y)
        {
            if (direction == Direction.Left)
            {
                _leftRelativePosition.X = x;
                _leftRelativePosition.Y = y;
            }
            else
            {
                _rightRelativePosition.X = x;
                _rightRelativePosition.Y = y;
            }
        }
    }
}
