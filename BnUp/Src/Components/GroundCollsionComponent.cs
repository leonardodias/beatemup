﻿using BnUp.Src.Enums;
using BnUp.Src.Tags;
using BnUp.Src.Util;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class GroundCollsionComponent : Component
    {
        public bool OnGround { get; private set; }
        private ZCoordComponent _zCoordComponent;

        private Hitbox testFindGroundHitbox;

        private const int GROUND_THRESHOLD = 1;

        public GroundCollsionComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            testFindGroundHitbox = new Hitbox(
                _zCoordComponent.BaseCollider.Width, 
                _zCoordComponent.BaseCollider.Height);

            base.EntityAwake();
        }

        public override void Update()
        {
            testFindGroundHitbox.Position.X = _zCoordComponent.BaseCollider.Position.X;
            testFindGroundHitbox.Position.Y = _zCoordComponent.BaseCollider.Position.Y + _zCoordComponent.ZValue;
            int higherGroundZ = 5000;

            foreach (var floorEntity in Scene[EntityTags.floorTag])
            {
                if (testFindGroundHitbox.Collide(floorEntity.Collider))
                {
                    TerrainTypeComponent floorComponent = floorEntity.Get<TerrainTypeComponent>();

                    bool reallyCollide = TerrainTypeUtil.CheckFloorType(testFindGroundHitbox, floorEntity, floorComponent.FloorType);

                    if (reallyCollide)
                    {
                        higherGroundZ = 0;
                    }
                }
            }

            float newZValue = _zCoordComponent.ZValue + _zCoordComponent.ZVelocity * Engine.DeltaTime;

            if (newZValue >= higherGroundZ && _zCoordComponent.ZValue < higherGroundZ)
            {
                _zCoordComponent.ZVelocity = (higherGroundZ - _zCoordComponent.ZValue) / Engine.DeltaTime;
            }

            if (_zCoordComponent.ZValue >= higherGroundZ && _zCoordComponent.ZValue < higherGroundZ + GROUND_THRESHOLD)
            {
                _zCoordComponent.ZValue = higherGroundZ;
                _zCoordComponent.ZVelocity = Math.Min(0, _zCoordComponent.ZVelocity);
            }

            OnGround = _zCoordComponent.ZValue == higherGroundZ;
        }
    }
}

