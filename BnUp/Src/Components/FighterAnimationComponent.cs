﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterAnimationComponent : Component
    {

        private VelocityComponent _velocityComponent;
        private DirectionComponent _directionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private FighterJumpComponent _fighterJumpComponent;
        private ZCoordComponent _zCoordComponent;
        private Image _image;
        private Sprite _sprite;

        public int LeftOffsetX { get; set; }
        public int RightOffsetX { get; set; }

        public FighterAnimationComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _fighterJumpComponent = this.Entity.Components.Get<FighterJumpComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _image = this.Entity.Components.Get<Image>();
            _sprite = this.Entity.Components.Get<Sprite>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            UpdateOffset();

            switch (_fighterAttributesComponent.State)
            {
                case Enums.FighterState.Faint:
                    if (_groundCollsionComponent.OnGround)
                        _sprite.Play("knockdown");
                    else
                        _sprite.Play("knockdown-fly");

                    break;
                case Enums.FighterState.KnockedOut:
                    if (_groundCollsionComponent.OnGround)
                        _sprite.Play("knockdown");
                    else
                        _sprite.Play("knockdown-fly");

                    break;

                case Enums.FighterState.Hurt:
                    _sprite.Play("hurt");

                    break;

                case Enums.FighterState.Grabbed:
                    _sprite.Play("grabbed");
                    break;

                case Enums.FighterState.GrabbedLifted:
                    _sprite.Play("grabbed-rear");
                    break;

                case Enums.FighterState.Grabbing:
                    _sprite.Play("grabbing");
                    break;

                case Enums.FighterState.Rising:
                    _sprite.Play("rising");
                    break;

                case Enums.FighterState.Dashing:
                    _sprite.Play("dashing");
                    break;

                case Enums.FighterState.Standing:

                    if (_fighterJumpComponent != null && _fighterJumpComponent.IsStartingJump())
                        _sprite.Play(_fighterJumpComponent.StartAnimationName);
                    else
                    {
                        if (_groundCollsionComponent.OnGround && _zCoordComponent.ZVelocity >= 0)
                        {
                            if (_groundCollsionComponent.OnGround &&
                                (Math.Abs(_velocityComponent.Velocity.X) > 0.1f
                                || Math.Abs(_velocityComponent.Velocity.Y) > 0.1f))
                            {
                                _sprite.Play("walking");
                            }
                            else
                            {
                                if (_groundCollsionComponent.OnGround && _zCoordComponent.ZVelocity >= 0)
                                {
                                    _sprite.Play("idle");
                                }
                            }
                        }
                        else
                            _sprite.Play("jump");

                    }

                    break;
            }

        }

        private void UpdateOffset()
        {
            if (_directionComponent != null)
            {
                if (_image != null)
                {
                    if (_directionComponent.Direction == Enums.Direction.Left)
                    {
                        _image.FlipX = true;
                        _image.X = LeftOffsetX;
                    }
                    else
                    {
                        _image.FlipX = false;
                        _image.X = RightOffsetX;
                    }
                }

                if (_sprite != null)
                {
                    if (_directionComponent.Direction == Enums.Direction.Left)
                    {
                        _sprite.FlipX = true;
                        _sprite.X = LeftOffsetX;
                    }
                    else
                    {
                        _sprite.FlipX = false;
                        _sprite.X = RightOffsetX;
                    }
                }

            }
        }
    }
}
