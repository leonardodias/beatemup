﻿using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterCameraLimitsComponent : Component
    {
        private bool _enabled = false;
        private CameraMovementComponent cameraMovementComponent;
        private Rectangle entityRectangle = new Rectangle();

        private ZCoordComponent _zCoordComponent;
        private FighterAttributesComponent _fighterAttributesComponent;

        public FighterCameraLimitsComponent() : base(true, false)
        {        
        }

        public override void EntityAwake()
        {
            _enabled = false;
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (cameraMovementComponent == null)
                cameraMovementComponent = Scene[EntityTags.cameraTag][0].Get<CameraMovementComponent>();
            else
            {
                if (_enabled)
                {
                    RestrictToCamera();
                }
                else
                {
                    entityRectangle.X = (int) this.Entity.Position.X;
                    entityRectangle.Y = (int)this.Entity.Position.Y;
                    entityRectangle.Width = (int)this.Entity.Width;
                    entityRectangle.Height = (int)this.Entity.Height;

                    if (cameraMovementComponent.GetCameraBounds().Contains(entityRectangle))
                    {
                        _enabled = true;
                    }
                }
            }

            base.Update();
        }

        private void RestrictToCamera()
        {
            _zCoordComponent.BaseCollider.Position.X
                = MathF.Max(_zCoordComponent.BaseCollider.Position.X, cameraMovementComponent.GetCameraBounds().X);
            _zCoordComponent.BaseCollider.Position.X
                = MathF.Min(_zCoordComponent.BaseCollider.Position.X
                , cameraMovementComponent.GetCameraBounds().X + cameraMovementComponent.GetCameraBounds().Width - _zCoordComponent.BaseCollider.Width);

            _zCoordComponent.BaseCollider.Position.Y
                = MathF.Max(_zCoordComponent.BaseCollider.Position.Y, cameraMovementComponent.GetCameraBounds().Y);
            _zCoordComponent.BaseCollider.Position.Y
                = MathF.Min(_zCoordComponent.BaseCollider.Position.Y
                , cameraMovementComponent.GetCameraBounds().Y + cameraMovementComponent.GetCameraBounds().Height - _zCoordComponent.BaseCollider.Height);
        }
    }
}
