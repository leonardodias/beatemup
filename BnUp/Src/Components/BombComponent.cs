﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BnUp.Src.Assets;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoBMFont;
using MonoBMFontUWP.Loader;
using Monocle;

namespace BnUp.Src.Components
{

    class BombComponent : Component
    {
        private bool active = true;
        private float explosionTime = 5;

        public BombComponent() : base(true, false)
        {
        }

        public override void Update()
        {
            if (active)
            {
                explosionTime -= Engine.DeltaTime;

                if (explosionTime <= 0)
                {
                    this.Entity.RemoveSelf();

                    CreateExplosion();
                }

            }
            
        }

        private void CreateExplosion()
        {
            Entity explosionEntity = new Entity();            
            explosionEntity.AddTag(EntityTags.effectTag);

            explosionEntity.Collider = new Hitbox(80, 80);       

            explosionEntity.Position.X = this.Entity.CenterX - explosionEntity.Width / 2;
            explosionEntity.Position.Y = this.Entity.CenterY - explosionEntity.Height / 2;

            Sprite sprite = AssetsManager.AllSprites.Create("explosao");
            sprite.Position.X -= 10;
            sprite.Position.Y -= 10;
            explosionEntity.Add(sprite);
            TTLComponent ttl = new TTLComponent();
            ttl.SetTTL(0.2f);
            explosionEntity.Add(ttl);

            ExplosionComponent explosionComponent = new ExplosionComponent();
            explosionComponent.Damage = 20;
            explosionEntity.Add(explosionComponent);

            Scene.Add(explosionEntity);
        }
    }
}
