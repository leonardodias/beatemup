﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class PlayerMovementComponent : Component
    {
        private PlayerInputComponent _playerInputComponent;

        private FighterMovementComponent _fighterMovementComponent;

        public bool OnAirMovement { get; set; }

        public PlayerMovementComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _fighterMovementComponent = this.Entity.Components.Get<FighterMovementComponent>();
            _playerInputComponent = this.Entity.Components.Get<PlayerInputComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (!_playerInputComponent.LockControls)
            {
                _fighterMovementComponent.MovementX = _playerInputComponent.VirtualJoystick.Value.X;
                _fighterMovementComponent.MovementY = _playerInputComponent.VirtualJoystick.Value.Y;
            }
        }
    }
}
