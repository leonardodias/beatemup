﻿using BnUp.Src.Assets;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class MicHookComponent : Component
    {
        Sprite _sprite;

        Image micHeadImage;
        Image micCableImage;
        Sprite micCableSprite;

        private float _maxHookDistance = 250;
        private float _velocity = 500;
        private DirectionComponent _directionComponent;

        private Vector2 micHeadPosition = new Vector2();
        private Vector2 micCablePosition = new Vector2();

        private bool switchFrames = false;
        private float switchFrameTime = 0.05f;
        private float currentSwitchFrameTime = 0.05f;

        private const int HeightGoing = 0;
        private const int HeightReturning = 3;

        private int state;

        private Entity _owner;
        private Sprite ownerSprite;

        private Hitbox baseHitbox;
        private Hitbox attackHitbox;

        private Entity enemyHooked;

        public MicHookComponent(Entity owner) : base(true, true)
        {
            _owner = owner;
            baseHitbox = new Hitbox(24, 8);
            attackHitbox = new Hitbox(24, 8);
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _sprite = AssetsManager.AllSprites.Create("microfone");

            micHeadImage = new Image(AssetsManager.TextureAtlas["michook-head"]);
            micCableImage = new Image(AssetsManager.TextureAtlas["michook-cable"]);
            micCableSprite = AssetsManager.AllSprites.Create("microfone");

            ownerSprite = _owner.Components.Get<Sprite>();

            enemyHooked = null;

            base.EntityAwake();
        }

        public override void Update()
        {
            switch (state)
            {
                case 0:
                    UpdateGoing();

                    break;
                case 1:
                    UpdateReturning();

                    break;
            }

            base.Update();
        }

        private void UpdateReturning()
        {
            this.Entity.Collider.Width -= _velocity * Engine.DeltaTime;

            if (_directionComponent.Direction == Enums.Direction.Left)
                this.Entity.X += _velocity * Engine.DeltaTime;


            micHeadPosition.X = this.Entity.CenterRight.X;
            micHeadPosition.Y = this.Entity.CenterRight.Y - HeightReturning;

            if (MathF.Abs(this.Entity.Collider.Width) < 20)
            {
                this.Entity.RemoveSelf();
                _owner.Get<FighterAttributesComponent>().State = Enums.FighterState.Standing;
            }

            if (enemyHooked != null)
            {
                if (_directionComponent.Direction == Enums.Direction.Left)
                    enemyHooked.Get<ZCoordComponent>().BaseCollider.Position.X = this.Entity.CenterLeft.X -
                        enemyHooked.Width;
                else
                    enemyHooked.Get<ZCoordComponent>().BaseCollider.Position.X = this.Entity.CenterRight.X;

                enemyHooked.Get<VelocityComponent>().SetVelocity(0, 0);
                enemyHooked.Get<FighterAttributesComponent>().State = Enums.FighterState.Hurt;
            }
        }

        private void UpdateGoing()
        {
            this.Entity.Collider.Width += _velocity * Engine.DeltaTime;

            if (_directionComponent.Direction == Enums.Direction.Left)
            {
                this.Entity.X -= _velocity * Engine.DeltaTime;
            }

            if (_directionComponent.Direction == Enums.Direction.Left)
                micHeadPosition.X = this.Entity.CenterLeft.X;
            else
                micHeadPosition.X = this.Entity.CenterRight.X - 48;

            micHeadPosition.Y = this.Entity.CenterRight.Y - HeightGoing;

            if (this.Entity.Collider.Width > _maxHookDistance)
            {
                SwitchToReturning();
            }
            else
            {
                if (_directionComponent.Direction == Enums.Direction.Left)
                    baseHitbox.Position.X = this.Entity.CenterLeft.X;
                else
                    baseHitbox.Position.X = this.Entity.CenterRight.X - baseHitbox.Width;

                //baseHitbox.Position.Y = this.Entity.Position.Y;
                baseHitbox.Position.Y = _owner.Get<ZCoordComponent>().BaseCollider.Position.Y;



                if (this.enemyHooked == null)
                {
                    foreach (var enemy in Scene[EntityTags.enemyTag])
                    {
                        if (enemy.Get<FighterAttributesComponent>().State == Enums.FighterState.Standing
                            || enemy.Get<FighterAttributesComponent>().State == Enums.FighterState.Attacking
                            || enemy.Get<FighterAttributesComponent>().State == Enums.FighterState.Hurt)

                            if (this.Entity.CollideCheck(enemy))
                            {
                                if (enemy.Components.Get<ZCoordComponent>().BaseCollider.Collide(baseHitbox))
                                {
                                    CatchEnemy(enemy);
                                    break;
                                }
                            }
                    }
                }
            }
        }

        private void CatchEnemy(Entity enemy)
        {
            this.enemyHooked = enemy;
            SwitchToReturning();
        }

        private void SwitchToReturning()
        {
            state = 1;
            ownerSprite.Play("special-attack-2");
        }

        public override void Render()
        {
            switch (state)
            {
                case 0:
                    RenderGoing();

                    break;
                case 1:
                    RenderReturn();

                    break;

            }

            base.Render();
        }

        private void RenderReturn()
        {
            int cableCount = (int)MathF.Floor(this.Entity.Collider.Width / 8);

            for (int i = 0; i < cableCount; i++)
            {
                micCablePosition.X = micHeadPosition.X - ((i + 1) * 8);
                micCablePosition.Y = micHeadPosition.Y;

                if (micCablePosition.X > this.Entity.X)
                    micCableSprite.GetFrame("cable", 2).Draw(micCablePosition);
            }
        }

        private void RenderGoing()
        {
            if (_directionComponent.Direction == Enums.Direction.Left)
                micHeadImage.Texture.Draw(micHeadPosition, micHeadImage.Origin, micHeadImage.Color, micHeadImage.Scale, micHeadImage.Rotation, Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally);
            else
                micHeadImage.Texture.Draw(micHeadPosition);

            int cableCount = (int)MathF.Floor(this.Entity.Collider.Width / 8);

            currentSwitchFrameTime -= Engine.DeltaTime;
            if (currentSwitchFrameTime <= 0)
            {
                switchFrames = !switchFrames;
                currentSwitchFrameTime += switchFrameTime;
            }

            int frame = switchFrames ? 0 : 1;

            for (int i = 0; i < cableCount; i++)
            {
                if (_directionComponent.Direction == Enums.Direction.Left)
                    micCablePosition.X = micHeadPosition.X + 48 + ((i + 1) * 9);
                else
                    micCablePosition.X = micHeadPosition.X - ((i + 1) * 9);

                micCablePosition.Y = micHeadPosition.Y;

                if ((_directionComponent.Direction == Enums.Direction.Right &&
                    micCablePosition.X > this.Entity.X)
                    ||
                    (_directionComponent.Direction == Enums.Direction.Left
                    && micCablePosition.X < this.Entity.X + this.Entity.Width)
                    )

                    micCableSprite.GetFrame("cable", frame).Draw(micCablePosition);

                if (frame == 0)
                    frame = 1;
                else
                    frame = 0;
            }
        }
    }
}
