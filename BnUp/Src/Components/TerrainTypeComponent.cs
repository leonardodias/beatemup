﻿using BnUp.Src.Enums;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class TerrainTypeComponent : Component
    {        
        public TerrainType FloorType { get; private set; }

        public TerrainTypeComponent(TerrainType floorType = TerrainType.Square) : base(false, false)
        {            
            FloorType = floorType;
        }
    }
}
