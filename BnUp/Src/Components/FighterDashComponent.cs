﻿using BnUp.Src.Data;
using BnUp.Src.Enums;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class FighterDashComponent : Component
    {
        private ZCoordComponent _zCoordComponent;
        private VelocityComponent _velocityComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private FighterMovementComponent _fighterMovementComponent;

        private int dashVelocity;
        private int currentDashVelocity;
        private float dashTime;
        private float currentDashTime;
        private bool dashActive = false;

        public FighterDashComponent(DashData dashData) : base(true, false)
        {
            dashVelocity = dashData.Velocity;
            dashTime = dashData.Time;
        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _fighterMovementComponent = this.Entity.Components.Get<FighterMovementComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (dashActive)
            {
                if (_fighterAttributesComponent.State != Enums.FighterState.Dashing
                    || !_groundCollsionComponent.OnGround)
                {
                    dashActive = false;
                }
                else
                {
                    currentDashTime -= Engine.DeltaTime;

                    if (currentDashTime > 0)
                        ApplyDash();
                    else
                    {
                        _fighterAttributesComponent.State = Enums.FighterState.Standing;
                        dashActive = false;
                    }

                }
            }
        }

        private void ApplyDash()
        {
            _velocityComponent.SetVelocity(currentDashVelocity, 0);
        }

        public void StartDash(Direction direction)
        {
            dashActive = true;
            currentDashTime = dashTime;
            _fighterAttributesComponent.State = Enums.FighterState.Dashing;

            if (direction == Direction.Left)
                currentDashVelocity = -dashVelocity;
            else
                currentDashVelocity = dashVelocity;
        }

        internal bool IsDashing()
        {
            return dashActive;
        }
    }
}
