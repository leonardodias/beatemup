﻿using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class ShadowComponent : Component
    {       
        private Entity _fighterEntity;
        private ZCoordComponent zCoordFighter;

        public ShadowComponent(Entity fighterEntity) : base(true, false)
        {
            _fighterEntity = fighterEntity;
        }

        public override void EntityAwake()
        {
            zCoordFighter = _fighterEntity.Get<ZCoordComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            bool visible = false;

            if (_fighterEntity.Visible && _fighterEntity.Active)
            {
                foreach (var floor in Scene[EntityTags.floorTag])
                {
                    if (this.Entity.CollideCheck(floor))
                        visible = true;
                }
            }            

            this.Entity.Visible = visible;           

            base.Update();
        }
    }
}
