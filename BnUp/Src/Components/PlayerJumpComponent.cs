﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class PlayerJumpComponent : Component
    {
        private ZCoordComponent _zCoordComponent;
        private VelocityComponent _velocityComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private PlayerInputComponent _playerInputComponent;
        private FighterMovementComponent _fighterMovementComponent;
        private Sprite _sprite;
        private FighterJumpComponent _fighterJumpComponent;

        public PlayerJumpComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _playerInputComponent = this.Entity.Components.Get<PlayerInputComponent>();
            _fighterMovementComponent = this.Entity.Components.Get<FighterMovementComponent>();
            _fighterJumpComponent = this.Entity.Components.Get<FighterJumpComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_groundCollsionComponent.OnGround
                && (_fighterAttributesComponent.State == Enums.FighterState.Standing
                || _fighterAttributesComponent.State == Enums.FighterState.Dashing))
            {
                if (!_playerInputComponent.LockControls && _playerInputComponent.JumpInput.Pressed)
                {
                    _fighterJumpComponent.StartJump();
                }              
            }
        }   

    }
}
