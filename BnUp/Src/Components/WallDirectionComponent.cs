﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class WallDirectionComponent : Component
    {
        public bool BlockUp { get; set; }
        public bool BlockDown { get; set; }
        public bool BlockLeft { get; set; }
        public bool BlockRight { get; set; }

        public WallDirectionComponent() : base(false, false)
        {  

        }
    }
}
