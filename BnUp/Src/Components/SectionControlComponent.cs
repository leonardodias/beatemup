﻿using BnUp.Src.Assets;
using BnUp.Src.Data;
using BnUp.Src.Factory;
using BnUp.Src.Tags;
using BnUp.Src.Util;
using Microsoft.Xna.Framework;
using Monocle;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class SectionControlComponent : Component
    {
        public Rectangle SectionArea { get; set; }
        public Rectangle TriggerArea { get; set; }
        public Rectangle NextLimits { get; set; }
        private bool active;

        private Entity camera;

        private List<CreateEnemyEventData> events = new List<CreateEnemyEventData>();
        private float delay;
        private int eventIndex;

        public SectionControlComponent(SectionData sectionData) : base(true, false)
        {
            SectionArea = new Rectangle(
                sectionData.Bounds.X,
                sectionData.Bounds.Y,
                sectionData.Bounds.Width,
                sectionData.Bounds.Height);
            TriggerArea = new Rectangle(
                sectionData.Trigger.X,
                sectionData.Trigger.Y,
                sectionData.Trigger.Width,
                sectionData.Trigger.Height);
            NextLimits = new Rectangle(
                sectionData.NextLimits.X,
                sectionData.NextLimits.Y,
                sectionData.NextLimits.Width,
                sectionData.NextLimits.Height
                );

            events.AddRange(sectionData.CreateEnemyEventDataList);
        }

        public override void EntityAwake()
        {
            base.EntityAwake();
        }

        public override void Update()
        {
            if (camera == null)
                camera = Scene[EntityTags.cameraTag][0];

            if (!active)
            {
                bool setActive = false;

                if (camera != null)
                {
                    if (TriggerArea.Contains(camera.Get<CameraMovementComponent>().GetCameraCenter()))
                        setActive = true;
                }

                if (setActive)
                {
                    active = true;                    
                    camera.Get<CameraMovementComponent>().Limits = SectionArea;
                }
            }
            else
            {
                LockPlayers();

                CheckEvents();
            }

        }

        private void LockPlayers()
        {
            foreach (var player in Scene[EntityTags.playerTag])
            {
                ZCoordComponent zCoord = player.Get<ZCoordComponent>();

                zCoord.BaseCollider.Position.X
                    = MathF.Max(zCoord.BaseCollider.Position.X, SectionArea.X);
                zCoord.BaseCollider.Position.X
                    = MathF.Min(zCoord.BaseCollider.Position.X
                    , SectionArea.X + SectionArea.Width - zCoord.BaseCollider.Width);

                zCoord.BaseCollider.Position.Y
                    = MathF.Max(zCoord.BaseCollider.Position.Y, SectionArea.Y);
                zCoord.BaseCollider.Position.Y
                    = MathF.Min(zCoord.BaseCollider.Position.Y
                    , SectionArea.Y + SectionArea.Height - zCoord.BaseCollider.Height);

            }
        }


        private void CheckEvents()
        {
            if (eventIndex >= events.Count)
            {
                if (Scene[EntityTags.enemyTag].Count == 0)
                    FinishSection();
            }
            else
            {
                delay += Engine.DeltaTime;

                if (delay > events[eventIndex].Delay
                    && Scene[EntityTags.enemyTag].Count < events[eventIndex].MinEnemiesCount)
                {
                    CreateEnemy(events[eventIndex]);
                    eventIndex++;
                    delay = 0;
                }
            }
        }

        private void CreateEnemy(CreateEnemyEventData createEnemyEventData)
        {
            string filePath = FightersPath.Instance.Get(createEnemyEventData.Name);
            FighterData testDataTwo =
                JObject.Parse(System.IO.File.ReadAllText(filePath)).ToObject<FighterData>();

            Entity enemyEntity = FighterFactory.CreateFighter(testDataTwo,
                createEnemyEventData.SpawnPoint.X,
                createEnemyEventData.SpawnPoint.Y, -2);
            Scene.Add(enemyEntity);
            
            Scene.Add(FighterFactory.CreateShadow(enemyEntity));

        }

        private void FinishSection()
        {
            camera.Get<CameraMovementComponent>().Limits = NextLimits;
            this.RemoveSelf();
        }
    }
}
