﻿using BnUp.Src.Assets;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class CreateMicHookComponent : Component
    {
        private DirectionComponent _directionComponent;
        private ZCoordComponent _zCoordComponent;
        private GroundCollsionComponent _groundCollsionComponent;
        private VelocityComponent _velocityComponent;
        private FighterAttributesComponent _fighterAttributesComponent;
        private Sprite _sprite;
        private PlayerInputComponent _playerInputComponent;

        private float currentAttackDelayTime = 0f;
        private float currentAttackDurationTime = 0f;
        private float currentAttackCooldownTime = 0f;
        private float attackDelayTime = 0.2f;
        private float attackDurationTime = 1f;
        private float attackCooldownTime = 0.2f;

        private int attackState = 0;
        private bool isAttacking = false;

        public CreateMicHookComponent() : base(true, false)
        {

        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _zCoordComponent = this.Entity.Components.Get<ZCoordComponent>();
            _groundCollsionComponent = this.Entity.Components.Get<GroundCollsionComponent>();
            _velocityComponent = this.Entity.Components.Get<VelocityComponent>();
            _fighterAttributesComponent = this.Entity.Components.Get<FighterAttributesComponent>();
            _sprite = this.Entity.Components.Get<Sprite>();
            _playerInputComponent = this.Entity.Components.Get<PlayerInputComponent>();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (isAttacking)
            {
                switch (attackState)
                {
                    case 0:

                        attackState = 1;

                        currentAttackDelayTime = attackDelayTime;

                        _fighterAttributesComponent.State = Enums.FighterState.Attacking;
                        _sprite.Play("special-attack-1");

                        break;
                    case 1:
                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {

                            _velocityComponent.SetVelocity(0, 0);


                            currentAttackDelayTime -= Engine.DeltaTime;

                            if (currentAttackDelayTime <= 0)
                            {
                                attackState = 2;
                                currentAttackDurationTime = attackDurationTime;

                                CreateMicHook();
                            }
                        }
                        else
                            isAttacking = false;

                        break;
                    case 2:

                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {
                            if (_groundCollsionComponent.OnGround)
                            {
                                _velocityComponent.SetVelocity(0, 0);
                            }

                            currentAttackDurationTime -= Engine.DeltaTime;

                            if (currentAttackDurationTime <= 0)
                            {
                                attackState = 3;

                                currentAttackCooldownTime = attackCooldownTime;

                                //AttackActive = false;
                            }
                        }
                        else
                            isAttacking = false;

                        break;
                    case 3:


                        if (_fighterAttributesComponent.State == Enums.FighterState.Attacking)
                        {
                            _velocityComponent.SetVelocity(0, 0);

                            currentAttackCooldownTime -= Engine.DeltaTime;

                            if (currentAttackCooldownTime <= 0)
                            {
                                isAttacking = false;
                                attackState = 0;

                                _fighterAttributesComponent.State = Enums.FighterState.Standing;
                            }
                        }
                        else
                            isAttacking = false;

                        break;
                }

            }
            else
            {
                attackState = 0;

                if (_fighterAttributesComponent.State == Enums.FighterState.Standing
                && _groundCollsionComponent.OnGround)
                {
                    if (_playerInputComponent.SpecialInput.Pressed)
                    {
                        isAttacking = true;
                    }
                }
            }


            base.Update();
        }

        private void CreateMicHook()
        {
            Entity hookEntity = new Entity();            
            hookEntity.Collider = new Hitbox(24, 12);

            TTLComponent tLComponent = new TTLComponent();
            tLComponent.SetTTL(2.5f);
            hookEntity.Add(tLComponent);

            if (_directionComponent.Direction == Enums.Direction.Left)
                hookEntity.X = this.Entity.X - hookEntity.Width;
            else
                hookEntity.X = this.Entity.X + _zCoordComponent.BaseCollider.Width;

            hookEntity.Y = this.Entity.Y + 7;

            //hookEntity.Add(AssetsManager.AllSprites.Create("microfone"));
            DirectionComponent directionComponent = new DirectionComponent();
            directionComponent.Direction = _directionComponent.Direction;
            hookEntity.Add(directionComponent);

            hookEntity.Add(new MicHookComponent(this.Entity));

            ZCoordComponent zCoord = new ZCoordComponent((int)hookEntity.Collider.Width, (int)hookEntity.Collider.Height);
            zCoord.BaseCollider.Position.X = hookEntity.X;
            zCoord.BaseCollider.Position.Y = _zCoordComponent.BaseCollider.Position.Y - 1;
            zCoord.ZValue = 0;
            zCoord.Active = false;
            hookEntity.Components.Add(zCoord);

            hookEntity.AddTag(EntityTags.zSortTag);
            //hookEntity.AddTag(EntityTags.effectTag);

            Scene.Add(hookEntity);
        }

    }
}
