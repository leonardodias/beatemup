﻿using BnUp.Src.Assets;
using BnUp.Src.Entities;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Components
{
    class ThrowAttackComponent : Component
    {
        private DirectionComponent _directionComponent;
        private FighterAttackComponent _throwedFigherAttackComponent;
        private FighterAttributesComponent _throwedFigherAttributesComponent;
        private GroundCollsionComponent _throwedFigherGroundCollisionComponent;

        private bool _enabled = true;

        public Entity Owner { get; set; }

        private Entity throwedFighter;
        public Entity ThrowedFighter
        {
            get { return throwedFighter; }
            set
            {
                throwedFighter = value;
                _throwedFigherAttackComponent = throwedFighter.Get<FighterAttackComponent>();
                _throwedFigherAttributesComponent = throwedFighter.Get<FighterAttributesComponent>();
                _throwedFigherGroundCollisionComponent = throwedFighter.Get<GroundCollsionComponent>();
            }
        }

        public int Damage { get; set; }

        private List<Entity> fightersHit = new List<Entity>();

        public ThrowAttackComponent() : base(true, false)
        {
        }

        public override void EntityAwake()
        {
            _directionComponent = this.Entity.Components.Get<DirectionComponent>();
            _enabled = true;
            fightersHit.Clear();

            base.EntityAwake();
        }

        public override void Update()
        {
            if (_throwedFigherGroundCollisionComponent != null &&
                _throwedFigherGroundCollisionComponent.OnGround)
            {
                _enabled = false;
                fightersHit.Clear();
            }

            if (_enabled)
            {
                foreach (var otherEntity in Scene[EntityTags.fighterTag])
                {
                    if (!otherEntity.Equals(this.Entity) && !otherEntity.Equals(Owner)
                        && !otherEntity.Equals(throwedFighter)
                        && !fightersHit.Contains(otherEntity))
                    {
                        FighterAttributesComponent targetAttributes = otherEntity.Get<FighterAttributesComponent>();

                        if (targetAttributes.State != Enums.FighterState.Faint
                            && targetAttributes.State != Enums.FighterState.KnockedOut)
                        {
                            bool collide =
                                this.Entity.Collider.Collide(otherEntity.Get<ZCoordComponent>().BaseCollider);

                            if (collide)
                            {
                                //effects
                                CreateImpactEffects();

                                FighterReceiveAttackComponent receiveAttackComponent =
                                    otherEntity.Components.Get<FighterReceiveAttackComponent>();

                                if (receiveAttackComponent != null)
                                {
                                    receiveAttackComponent.GetHit
                                        (Damage,
                                        true,
                                        _directionComponent.Direction);
                                }

                                fightersHit.Add(otherEntity);
                            }
                        }
                    }
                }
            }
        }

        private void CreateImpactEffects()
        {
            AssetsManager.GetSound(Enums.SoundKey.PunchImpact).Play();

            ImpactEntity impactEntity = Engine.Pooler.Create<ImpactEntity>();
            impactEntity.Position.X = this.Entity.X;
            impactEntity.Position.Y = this.Entity.Y - 45;
            Scene.Add(impactEntity);
        }
    }
}
