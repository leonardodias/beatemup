﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System.Collections.Generic;

namespace BnUp.Src.Scenes
{
    class SelectPlayerScene : Scene
    {
        private class GamePadController
        {
            private VirtualJoystick VirtualJoystick { get; set; }
            private VirtualButton SelectButton { get; set; }

            private bool pressedLeft;
            private bool pressedRight;

            public GamePadController(int gamePadIndex, bool setupKeys = false)
            {
                SetupInput(gamePadIndex, setupKeys);
            }

            private void SetupInput(int gamePadIndex, bool setupKeys = false)
            {
                VirtualJoystick = new VirtualJoystick(true);

                if (setupKeys)
                    VirtualJoystick.Nodes.Add(new VirtualJoystick.KeyboardKeys(
                        VirtualInput.OverlapBehaviors.TakeNewer,
                        Keys.A,
                        Keys.D,
                        Keys.W,
                        Keys.S));

                VirtualJoystick.Nodes.Add(new VirtualJoystick.PadLeftStick(gamePadIndex, 0.4f));
                VirtualJoystick.Nodes.Add(new VirtualJoystick.PadDpad(gamePadIndex));

                SelectButton = new VirtualButton();

                if (setupKeys)
                    SelectButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.K));

                SelectButton.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.A));
            }

            public void AfterUpdate()
            {
                pressedRight = VirtualJoystick.Value.X > 0.5f;
                pressedLeft = VirtualJoystick.Value.X < -0.5f;
            }

            public bool pressRight()
            {
                return VirtualJoystick.Value.X > 0.5f && !pressedRight;
            }

            public bool pressLeft()
            {
                return VirtualJoystick.Value.X < -0.5f && !pressedLeft;
            }

            public bool pressSelect()
            {
                return SelectButton.Pressed;
            }
        }

        Camera camera; // Camera handler (so that we can modify it's paramteres when needed, like zoom)

        List<GamePadController> gamePadControllers = new List<GamePadController>();
        List<Entity> avatares = new List<Entity>();
        private bool[] activePlayers = new bool[4];
        private bool[] finishSelection = new bool[4];
        private int[] playersIndex = new int[4];

        private Dictionary<int, string> animationDictionary = new Dictionary<int, string>();
        private Dictionary<int, string> filenameDictionary = new Dictionary<int, string>();

        public override void Begin()
        {
            camera = new Camera(Engine.Width, Engine.Height);

            EverythingRenderer renderer = new EverythingRenderer();
            renderer.Camera = camera; // Attach the new camera to the EverythingRenderer
            renderer.SamplerState = SamplerState.PointClamp;

            Add(renderer);

            gamePadControllers.Add(new GamePadController(0, true));
            gamePadControllers.Add(new GamePadController(1, false));
            gamePadControllers.Add(new GamePadController(2, false));
            gamePadControllers.Add(new GamePadController(3, false));

            activePlayers[0] = true;

            for (int i = 0; i < 4; i++)
            {
                Entity avatarEntity = new Entity();
                avatarEntity.Position.X = 30 + 14 + 89 * i + 10;
                avatarEntity.Position.Y = 100;

                avatarEntity.Add(AssetsManager.AllSprites.Create("selectplayer"));

                avatares.Add(avatarEntity);
                Add(avatarEntity);
            }

            animationDictionary.Add(0, "vocal");
            animationDictionary.Add(1, "baixista");
            animationDictionary.Add(2, "guitarrista");
            animationDictionary.Add(3, "baterista");

            filenameDictionary.Add(0, "./Content/Data/Fighters/VocalFighterData.json");
            filenameDictionary.Add(1, "./Content/Data/Fighters/BassFighterData.json");
            filenameDictionary.Add(2, "./Content/Data/Fighters/GuitarFighterData.json");
            filenameDictionary.Add(3, "./Content/Data/Fighters/DrumFighterData.json");


            Entity text = new Entity();
            text.Position.X = 10;
            text.Position.Y = 10;

            BMFontComponent bmfontComponent = new BMFontComponent();
            bmfontComponent.Text = "Select Player";
            bmfontComponent.Font = AssetsManager.GetFont(Enums.FontKey.ZCool24);
            text.Add(bmfontComponent);
            Add(text);
        }

        public override void Update()
        {
            for (int i = 1; i < 4; i++)
            {
                GamePadState gamePadState = GamePad.GetState(i);
                activePlayers[i] = gamePadState.IsConnected;

                avatares[i].Visible = activePlayers[i];
            }

            bool allFinished = true;

            for (int i = 0; i < 4; i++)
                if (activePlayers[i] && !finishSelection[i])
                    allFinished = false;

            if (allFinished)
            {
                CloseScene();
            }
            else
            {
                UpdateSelection();
            }

            for (int i = 0; i < 4; i++)
            {
                
            }

            base.Update();
        }

        private void UpdateSelection()
        {
            for (int i = 0; i < 4; i++)
            {
                if (finishSelection[i])
                {


                }
                else
                {
                    if (gamePadControllers[i].pressLeft())
                    {
                        playersIndex[i]--;
                        if (playersIndex[i] < 0)
                            playersIndex[i] = 3;

                        avatares[i].Get<Sprite>().Play(animationDictionary[playersIndex[i]]);
                    }

                    if (gamePadControllers[i].pressRight())
                    {
                        playersIndex[i]++;
                        if (playersIndex[i] >= 4)
                            playersIndex[i] = 0;

                        avatares[i].Get<Sprite>().Play(animationDictionary[playersIndex[i]]);
                    }

                    if (gamePadControllers[i].pressSelect())
                    {
                        finishSelection[i] = true;
                    }
                }
            }

        }

        private void CloseScene()
        {
            List<string> selectedFilenames = new List<string>();
            for (int i = 0; i < 4; i++)
            {
                if (activePlayers[i])
                    selectedFilenames.Add(filenameDictionary[playersIndex[i]]);

            }

            Engine.Scene = new TestScene(selectedFilenames);

        }

        public override void AfterUpdate()
        {
            foreach (var item in gamePadControllers)
            {
                item.AfterUpdate();
            }

            base.AfterUpdate();
        }
    }
}
