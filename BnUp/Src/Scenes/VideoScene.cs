﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Monocle;
using System.Collections.Generic;
using BnUp.Src.Tags;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Scenes
{
    class VideoScene : Scene
    {
        private class GamePadController
        {
            private VirtualButton SelectButton { get; set; }

            public GamePadController(int gamePadIndex, bool setupKeys = false)
            {
                SetupInput(gamePadIndex, setupKeys);
            }

            private void SetupInput(int gamePadIndex, bool setupKeys = false)
            {
                SelectButton = new VirtualButton();

                if (setupKeys)
                    SelectButton.Nodes.Add(new VirtualButton.KeyboardKey(Keys.K));

                SelectButton.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.A));
            }

            public bool pressSelect()
            {
                return SelectButton.Pressed;
            }
        }

        Camera camera; // Camera handler (so that we can modify it's paramteres when needed, like zoom)

        List<GamePadController> gamePadControllers = new List<GamePadController>();

        private bool[] activePlayers = new bool[4];

        Video video;
        Texture2D videoTexture;
        VideoPlayer videoPlayer;

        public override void Begin()
        {
            camera = new Camera(Engine.Width, Engine.Height);

            EverythingRenderer renderer = new EverythingRenderer();
            renderer.Camera = camera; // Attach the new camera to the EverythingRenderer
            renderer.SamplerState = SamplerState.PointClamp;

            Add(renderer);

            gamePadControllers.Add(new GamePadController(0, true));
            gamePadControllers.Add(new GamePadController(1, false));
            gamePadControllers.Add(new GamePadController(2, false));
            gamePadControllers.Add(new GamePadController(3, false));

            activePlayers[0] = true;

            video = AssetsManager.Instance.LoadVideo("outputmp");
            videoPlayer = new VideoPlayer();

        }

        public override void Update()
        {
            for (int i = 1; i < 4; i++)
            {
                GamePadState gamePadState = GamePad.GetState(i);
                activePlayers[i] = gamePadState.IsConnected;
            }

            if (videoPlayer.State == MediaState.Stopped)
            {
                videoPlayer.Play(video);
            }

            base.Update();
        }

        public override void Render()
        {
            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Matrix * Engine.ScreenMatrix);            

            if (videoPlayer.State != MediaState.Stopped)
                videoTexture = videoPlayer.GetTexture();

            if (videoTexture != null)
            {
                Draw.SpriteBatch.Draw(videoTexture, Engine.Viewport.Bounds, Color.White);
                videoTexture.Dispose();
            }

            Draw.SpriteBatch.End();

            //base.Render();  
        }

        public override void End()
        {
            videoPlayer.Dispose();
            base.End();
        }

    }
}
