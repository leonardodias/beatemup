﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Monocle;
using System.Collections.Generic;
using BnUp.Src.Tags;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Scenes
{
    class StartScene : Scene
    {
        Camera camera;
        private Texture2D titleTexture;

        public override void Begin()
        {
            camera = new Camera(Engine.Width, Engine.Height);

            EverythingRenderer renderer = new EverythingRenderer();
            renderer.Camera = camera; // Attach the new camera to the EverythingRenderer
            renderer.SamplerState = SamplerState.PointClamp;

            Add(renderer);
            titleTexture = Engine.Instance.Content.Load<Texture2D>(@"Images\title");
        }

        public override void Update()
        {

            if (HasInputChanged()
                || (Keyboard.GetState().IsKeyDown(Keys.Space)))
                Engine.Scene = new SelectPlayerScene();


            base.Update();
        }

        public override void Render()
        {
            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Matrix * Engine.ScreenMatrix);
            Draw.SpriteBatch.Draw(titleTexture, camera.Viewport.Bounds, Color.White);

            Draw.SpriteBatch.End();
        }

        private bool HasInputChanged()
        {
            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                var buttonList = new List<Buttons>()
                {
                    {Buttons.A},
                    {Buttons.B},
                    {Buttons.Y},
                    {Buttons.X},
                    {Buttons.Start},
                    {Buttons.Back},
                    {Buttons.RightShoulder},
                    {Buttons.LeftShoulder},
                    {Buttons.RightTrigger},
                    {Buttons.LeftTrigger}
                };

                foreach (var button in buttonList)
                {
                    if (GamePad.GetState(PlayerIndex.One).IsButtonDown(button))
                        return true;
                }
            }

            return false;
        }

        public override void End()
        {
            titleTexture.Dispose();
            base.End();
        }

    }
}




