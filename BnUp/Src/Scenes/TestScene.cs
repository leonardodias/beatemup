﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using BnUp.Src.Controllers;
using BnUp.Src.Data;
using BnUp.Src.Entities;
using BnUp.Src.Factory;
using BnUp.Src.Map;
using BnUp.Src.Renderers;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Monocle;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Scenes
{
    class TestScene : Scene
    {
        Camera camera; // Camera handler (so that we can modify it's paramteres when needed, like zoom)
        Camera hudCamera;

        List<string> _playersFilename = new List<string>();

        BMFontComponent bmfontComponent;

        List<PauseController> pauseControllers = new List<PauseController>();
        PauseController activePauseController = null;
        MyCustomRenderer renderer;

        public TestScene(List<string> playersFilename) : base()
        {
            _playersFilename.AddRange(playersFilename);
        }

        public override void Begin()
        {
            base.Begin();

            renderer = new MyCustomRenderer();
            renderer.SamplerState = SamplerState.PointClamp;
            renderer.DebugMode = false;

            Add(renderer);

            // Setup the camera
            camera = new Camera(Engine.Width, Engine.Height);
            //camera.CenterOrigin();
            //camera.Zoom = 1.5f;
            renderer.Camera = camera; // Attach the new camera to the EverythingRenderer

            hudCamera = new Camera(Engine.Width, Engine.Height);
            renderer.HudCamera = hudCamera;

            //novo stage
            string stageJson = System.IO.File.ReadAllText("./Content/Data/Stages/TestStageC.json");
            StageData stageData = JObject.Parse(stageJson).ToObject<StageData>();

            foreach (var backgroundData in stageData.Backgrounds)
                Add(StageFactory.CreateStageBackground(backgroundData));
            foreach (var wallData in stageData.Walls)
                Add(StageFactory.CreateWall(wallData));
            foreach (var floorData in stageData.Floors)
                Add(StageFactory.CreateFloor(floorData));
            foreach (var sectionData in stageData.Sections)
                Add(StageFactory.CreateSection(sectionData));
            foreach (var stageEntityData in stageData.Entities)
                Add(StageFactory.CreateStageTriggerEntity(stageEntityData));

            //Entity musicEntity = StageFactory.CreateMusicEntity(stageData.Music);
            //Add(musicEntity);
            Song song = AssetsManager.Instance.LoadSong(stageData.Music.Name);
            TimeData loopStart = stageData.Music.LoopStart;

            MusicController.Instance.Play(song, loopStart);

            int index = 0;
            foreach (var filename in _playersFilename)
            {
                string fighterJsonData = System.IO.File.ReadAllText(filename);
                FighterData playerData = JObject.Parse(fighterJsonData).ToObject<FighterData>();
                Entity pEntity = FighterFactory.CreateFighter(playerData, stageData.PlayersStart[index].X, stageData.PlayersStart[index].Y, stageData.PlayersStart[index].Z, index);
                Add(pEntity);
                Add(FighterFactory.CreateShadow(pEntity, playerData.ShadowX));                
                //pauseControllers.MusicEntity = musicEntity;

                pauseControllers.Add(new PauseController(index));

                Entity hudLifeBar = new Entity();
                hudLifeBar.AddTag(EntityTags.hudElementsTag);
                hudLifeBar.Add(new HudLifebarComponent(index, pEntity));
                Add(hudLifeBar);

                index++;
            }

            /*
            string bassFighterJsonData = System.IO.File.ReadAllText("./Content/Data/Fighters/BassFighterData.json");
            FighterData bassPlayerData = JObject.Parse(bassFighterJsonData).ToObject<FighterData>();
            Entity pBEntity = FighterFactory.CreateFighter(bassPlayerData, stageData.PlayersStart[1].X, stageData.PlayersStart[1].Y, stageData.PlayersStart[1].Z, 1);
            Add(pBEntity);
            Add(FighterFactory.CreateShadow(pBEntity));

            string vocalFighterJsonData = System.IO.File.ReadAllText("./Content/Data/Fighters/VocalFighterData.json");
            FighterData vocalPlayerData = JObject.Parse(vocalFighterJsonData).ToObject<FighterData>();
            Entity pEntity = FighterFactory.CreateFighter(vocalPlayerData, stageData.PlayersStart[0].X, stageData.PlayersStart[0].Y, stageData.PlayersStart[0].Z);
            Add(pEntity);
            Add(FighterFactory.CreateShadow(pEntity));

            string guitarFighterJsonData = System.IO.File.ReadAllText("./Content/Data/Fighters/GuitarFighterData.json");
            FighterData guitarPlayerData = JObject.Parse(guitarFighterJsonData).ToObject<FighterData>();
            Entity pGEntity = FighterFactory.CreateFighter(guitarPlayerData, stageData.PlayersStart[2].X, stageData.PlayersStart[2].Y, stageData.PlayersStart[2].Z);
            Add(pGEntity);
            Add(FighterFactory.CreateShadow(pGEntity));
            */
            /*
            Add(new FloorEntity(0, 96, 96, 96, Enums.TerrainType.Square));
            Add(new FloorEntity(96 + 32, 96, 96, 96, Enums.TerrainType.Square));
            Add(new FloorEntity(96 + 32 + 96, 96 + 32, 96, 96 -32, Enums.TerrainType.Square));
            Add(new WallEntity (96 + 32 + 96 + 32, 96, 32, 32));
            Add(new FloorEntity(96 + 32 + 96 + 32, 96 - 32, 32, 32, Enums.TerrainType.Square));

            Add(new WallEntity(0, 0, 32 * 12, 64));
            Add(new WallEntity(0, 32, 32 * 7, 64));
            */


            //Add(new FloorEntity    (0     , 32 * 3, 32 * 5, 96, Enums.TerrainType.Square));
            //Add(new FloorEntity    (32 * 5, 32 * 3, 32 * 3, 96, Enums.TerrainType.TriangleSW));
            //Add(new WallRightEntity(32 * 7,32 * 3, 32 * 1, 96, Enums.TerrainType.TriangleNE));

            

            CameraEntity c = new CameraEntity(camera);
            c.Get<CameraMovementComponent>().Limits = new Rectangle(
                stageData.CameraLimits.X,
                stageData.CameraLimits.Y,
                stageData.CameraLimits.Width,
                stageData.CameraLimits.Height);
            Add(c);

            SpriteFont font = Engine.Instance.Content.Load<SpriteFont>(@"Font\" + "File");

            Entity text = new Entity();
            text.Position.X = 80;
            text.Position.Y = 80;
            text.Add(new Text(font, "Lalalala", new Vector2(10, 10)));

            bmfontComponent = new BMFontComponent();
            bmfontComponent.Text = "Hello, Worlde";
            bmfontComponent.Font = AssetsManager.GetFont(Enums.FontKey.ZCool24);
            text.Add(bmfontComponent);

            text.AddTag(EntityTags.hudElementsTag);

            Entity screenOpening = new Entity();
            screenOpening.AddTag(EntityTags.hudElementsTag);
            screenOpening.Add(new OpenSceneEffectComponent());
            Add(screenOpening);

            //Add(text);
        }

        public override void Update()
        {
            //bmfontComponent.Text = testPlayer.Position.ToString();

            if (Paused)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.B))
                    renderer.DebugMode = true;
                if (Keyboard.GetState().IsKeyDown(Keys.N))
                    renderer.DebugMode = true;

                if (Keyboard.GetState().IsKeyDown(Keys.F3))
                    Engine.Scene = new SelectPlayerScene();


                if (activePauseController == null || activePauseController.PausePressed())
                {
                    activePauseController = null;
                    Paused = false;
                    MusicController.Instance.Resume();

                }
            }
            else
            {
                base.Update();

                foreach (var item in pauseControllers)
                {
                    if (item.PausePressed())
                    {
                        Paused = true;
                        activePauseController = item;                        

                        MusicController.Instance.Pause();
                    }

                    break;
                }
            }
        }
    }

    class PauseController
    {
        private int GamePadIndex { get; set; }
        private VirtualButton PauseInput { get; set; }

        public PauseController(int gamePadIndex)
        {
            PauseInput = new VirtualButton();
            PauseInput.Nodes.Add(new VirtualButton.KeyboardKey(Keys.P));
            PauseInput.Nodes.Add(new VirtualButton.PadButton(gamePadIndex, Buttons.Start));
        }

        internal bool PausePressed()
        {
            return PauseInput.Pressed;
        }
    }
}
