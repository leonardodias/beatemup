﻿using BnUp.Src.Components;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    class FloorEntity : Entity
    {
        public FloorEntity(int x, int y, int w, int h, TerrainType type = TerrainType.Square)
        {
            AddTag(EntityTags.floorTag);

            Position.X = x;
            Position.Y = y;

            Collider = new Hitbox(w, h, 0, 0);

            Add(new TerrainTypeComponent(type));
        }
    }
}
