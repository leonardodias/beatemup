﻿using BnUp.Src.Components;
using BnUp.Src.Enums;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    class WallRightEntity : Entity
    {
        public WallRightEntity(int x, int y, int w, int h, TerrainType type = TerrainType.Square)
        {
            AddTag(EntityTags.blockTag);

            Position.X = x;
            Position.Y = y;

            Collider = new Hitbox(w, h, 0, 0);

            Add(new TerrainTypeComponent(type));

            WallDirectionComponent wallDirectionComponent = new WallDirectionComponent();
            wallDirectionComponent.BlockLeft = false;
            wallDirectionComponent.BlockRight = true;
            wallDirectionComponent.BlockUp = true;
            wallDirectionComponent.BlockDown = true;
            Add(wallDirectionComponent);

        }
    }
}
