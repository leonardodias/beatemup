﻿using BnUp.Src.Components;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    [Pooled]
    class AttackEntity : Entity
    {
        public AttackEntity() : this(0, 0, 48, 8)
        {
        }

        public AttackEntity(int X, int Y, int w, int h)
        {
            Collider = new Hitbox(w, h, 0, 0);

            AddTag(EntityTags.attackTag);

            Add(new TTLComponent());
            Add(new DirectionComponent());
            Add(new EntityPinComponent());
            Add(new AttackComponent());            
        }

        public override void Added(Scene scene)
        {         
            base.Added(scene);
        }
    }
}
