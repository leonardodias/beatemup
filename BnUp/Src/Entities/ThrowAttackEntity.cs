﻿using BnUp.Src.Components;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    [Pooled]
    class ThrowAttackEntity : Entity
    {
        public ThrowAttackEntity() : this(0, 0, 48, 8)
        {
        }

        public ThrowAttackEntity(int X, int Y, int w, int h)
        {
            Collider = new Hitbox(w, h, 0, 0);

            AddTag(EntityTags.attackTag);

            Add(new ThrowAttackComponent());
            Add(new DirectionComponent());
        }

        public override void Added(Scene scene)
        {
            base.Added(scene);
        }
    }
}
