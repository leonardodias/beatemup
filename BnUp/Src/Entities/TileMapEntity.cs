﻿using BnUp.Src.Components;
using BnUp.Src.Map;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{ 
    class TileMapEntity : Entity
    {
        private TileMap _tileMap;

        public TileMapEntity(TileMap tileMap)
        {
            _tileMap = tileMap;
            this.Components.Add(new TileMapComponent(tileMap));
        }

        public override void Removed(Scene scene)
        {
            _tileMap.Dispose();
        }
    }
}
