﻿using BnUp.Src.Components;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    class CameraEntity : Entity
    {
        private Camera _camera;

        public CameraEntity(Camera camera)
        {
            _camera = camera;

            AddTag(EntityTags.cameraTag);

            Add(new CameraMovementComponent(camera));
            Add(new Shaker(false, ShakePosition));
            

        }

        public void ShakePosition(Vector2 shake)
        {
            _camera.X += shake.X;
            _camera.Y += shake.Y;
        }

    }
}
