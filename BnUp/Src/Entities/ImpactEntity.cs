﻿using BnUp.Src.Assets;
using BnUp.Src.Components;
using BnUp.Src.Tags;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Entities
{
    [Pooled]
    class ImpactEntity : Entity
    {
        Sprite _sprite;

        public ImpactEntity() : this(0, 0, 48, 48)
        {
        }

        public ImpactEntity(int X, int Y, int w, int h)
        {
            Collider = new Hitbox(32, 32);

            AddTag(EntityTags.effectTag);

            TTLComponent ttlComponent = new TTLComponent();
            ttlComponent.SetTTL(0.15f);
               
            Add(ttlComponent);

            _sprite = AssetsManager.AllSprites.Create("impact");
            Add(_sprite);
        }

        public override void Added(Scene scene)
        { 
            _sprite.Play("spark", false);
            base.Added(scene);
        }
    }
}
