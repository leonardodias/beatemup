﻿using BnUp.Src.Data;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Controllers
{
    class MusicController
    {
        #region Singleton

        private static MusicController _instance = null;

        public static MusicController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new MusicController();
                return _instance;
            }
        }

        private MusicController()
        {
            MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;
        }
        #endregion

        private Song _currentSong;
        private TimeSpan _currentPauseTime;
        private TimeData _loopStart;
        private bool _pause;
        private bool _enabled = false;

        public void Play(Song song)
        {
            _currentSong = song;
            if (_enabled)
                MediaPlayer.Play(song);
        }

        public void Play(Song song, TimeData loopStart)
        {
            if (_enabled)
                this.Play(song);
            this.SetLoopStart(loopStart);            
        }

        public void Pause()
        {
            if (_currentSong != null)
            {
                _currentPauseTime = MediaPlayer.PlayPosition;
                _pause = true;
                MediaPlayer.Stop();
            }
        }

        public void Resume()
        {
            _pause = false;

            if (_enabled && _currentSong != null && _currentPauseTime != null)
            {
                MediaPlayer.Play(_currentSong, _currentPauseTime);
            }
        }

        public void SetLoopStart(TimeData timeData)
        {
            _loopStart = timeData;
        }

        private void MediaPlayer_MediaStateChanged(object sender, EventArgs e)
        {
            if (MediaPlayer.State == MediaState.Stopped
                && _loopStart != null
                && _currentSong != null
                && !_pause
                && _enabled)
                MediaPlayer.Play(_currentSong, new TimeSpan(0, 0, _loopStart.Minute, _loopStart.Seconds, _loopStart.Milliseconds));
        }
    }
}
