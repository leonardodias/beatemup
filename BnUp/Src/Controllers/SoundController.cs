﻿using BnUp.Src.Assets;
using BnUp.Src.Enums;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnUp.Src.Controllers
{
    class SoundController
    {
        #region Singleton

        private static SoundController _instance = null;

        public static SoundController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SoundController();
                return _instance;
            }
        }

        private SoundController()
        {            
        }

        private  Dictionary<SoundKey, SoundEffectInstance> soundInstances = new Dictionary<SoundKey, SoundEffectInstance>();


        internal void PlaySound(SoundKey key)
        {
            if (!soundInstances.ContainsKey(key)) {
                SoundEffectInstance instance = AssetsManager.GetSound(key).CreateInstance();
                soundInstances.Add(key, instance);
            }

            soundInstances[key].Play();
        }
        #endregion

    }
}
