﻿using BnUp.Src.Assets;
using BnUp.Src.Scenes;
using BnUp.Src.Tags;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monocle;

namespace BnUp
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class BnUpGame : Engine
    {
        public BnUpGame() : base(
            width: 480,
            height: 270,
            windowWidth: 960,
            windowHeight: 540,
            windowTitle: "Beat n Up Game",
            fullscreen: false
            )
        {
            //ClearColor = Color.CornflowerBlue;
            ClearColor = Color.Black;

            IsMouseVisible = true;
            IsFixedTimeStep = true;

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            AssetsManager assetsManager = AssetsManager.Instance;

            //Commands.Open = true;
            int id = EntityTags.playerTag.ID;
            id = EntityTags.enemyTag.ID;
            id = EntityTags.fighterTag.ID;


            //Scene = new TestScene();
            //Scene = new SelectPlayerScene();
            //Scene = new VideoScene();
            Scene = new StartScene();

        }

        protected override void UnloadContent()
        {
            base.UnloadContent();            
        }
    }
}
