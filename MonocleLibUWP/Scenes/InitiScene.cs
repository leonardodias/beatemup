﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonocleLibUWP.Scenes
{
    public class InitScene : Scene
    {
        Camera camera; // Camera handler (so that we can modify it's paramteres when needed, like zoom)
                       //Dummy dum; // Dummy

        public InitScene() : base()
        { }

        public override void Begin()
        {
            base.Begin();

            // Add the EverythingRenderer object so it will actually display the contents of the scene
            EverythingRenderer er = new EverythingRenderer();
            Add(er);

            // Setup the camera
            camera = new Camera(640, 360);
            camera.CenterOrigin();
            camera.Zoom = 1.5f;
            er.Camera = camera; // Attach the new camera to the EverythingRenderer
        }
    }
}
