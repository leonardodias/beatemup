﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Monocle;
using MonocleLibUWP.Scenes;

namespace MonocleLibUWP
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Engine
    {        
        public Game1() : base(
            width: 400,
            height: 240,
            windowWidth: 800,
            windowHeight: 480,
            windowTitle: "Monocle Game",
            fullscreen: false
            )
        {
            Engine.ClearColor = Color.CornflowerBlue;
            Content.RootDirectory = "Content";
            ViewPadding = -32;
        }

        protected override void Initialize()
        {
            base.Initialize();
            InitScene isc = new InitScene();
            Scene = isc;            
        }

        protected override void LoadContent()
        {
            // TODO: use this.Content to load your game content here

            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here

            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
